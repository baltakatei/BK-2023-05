/* Desc:
 * Usage: ./test
 * Ref/Attrib: [1] https://youtu.be/ix5jPkxsr7M?t=3072
 *             [2] C: Multiple scanf's, when I enter in a value for one scanf it skips the second scanf https://stackoverflow.com/a/9562355
 */


#include <stdio.h>
#include <stdlib.h>


int main()
{
  char name[20]; // storing string as an array of chars
  printf("Enter your name: ");
  scanf("%s", name); // lf tells scanf you are looking for adouble. drops chars after space. 
  //getchar();
  printf("Your name is %s\n", name);
  
  printf("\n");
  return 0;
};

/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
