/* Desc: Function example
 * Usage: ./test
 * Ref/Attrib: [0] C programming tutorial for beginners https://youtu.be/KJgsSFOSQv0?t=5814
 *             [1] https://youtu.be/ix5jPkxsr7M?t=3072
 *             [2] C: Multiple scanf's, when I enter in a value for one scanf it skips the second scanf https://stackoverflow.com/a/9562355
 */


#include <stdio.h>
#include <stdlib.h>

void sayHi(char name[], int age){
  printf("Hello %s, you are %d.\n", name, age);
};

int main()
{
  printf("Top\n");
  sayHi("Balta Katei", 671);
  sayHi("Kodaw Kuori", 350);
  sayHi("Samki Licui", 21);
  printf("bottom\n");
  return 0;
};


/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
