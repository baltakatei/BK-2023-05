/* Desc: Return example
 * Usage: ./test
 * Ref/Attrib: [0] C programming tutorial for beginners https://youtu.be/KJgsSFOSQv0?t=6338
 *             [1] https://youtu.be/ix5jPkxsr7M?t=3072
 *             [2] C: Multiple scanf's, when I enter in a value for one scanf it skips the second scanf https://stackoverflow.com/a/9562355
 */


#include <stdio.h>
#include <stdlib.h>

double cube(double num);

int main(){
  printf("Answer: %f", cube(7.0));
  return 0;
};

double cube(double num){
  return num * num * num;
  printf("I never get displayed.");
};


/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
