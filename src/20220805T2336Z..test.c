/* Desc:
 * Usage: ./test
 * Ref/Attrib: [1] https://youtu.be/ix5jPkxsr7M?t=3072
 *             [2] C: Multiple scanf's, when I enter in a value for one scanf it skips the second scanf https://stackoverflow.com/a/9562355
 */


#include <stdio.h>
#include <stdlib.h>


int main()
{
  int age;
  printf("Enter your age: ");
  scanf("%d", &age); // scanf is like opposite of printf
  getchar(); // Need to run getchar() after each scanf() in order to clear
             // scanf() input buffer for next scanf() command.
  printf("You are %d years old\n", age);

  double gpa;
  printf("Enter your gpa: ");
  scanf("%lf", &gpa); // lf tells scanf you are looking for adouble
  getchar();
  printf("Your GPA is %f\n", gpa);

  char grade;
  printf("Enter your grade: ");
  scanf("%c", &grade); // lf tells scanf you are looking for adouble
  getchar();
  printf("Your grade is %c\n", grade);
  
  printf("\n");
  return 0;
};

/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
