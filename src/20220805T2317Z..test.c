/* Desc:
 * Usage: ./test
 * Ref/Attrib: [1] https://youtu.be/ix5jPkxsr7M?t=3072
 */

#include <stdio.h>
#include <stdlib.h>


int main()
{
  const int FAV_NUM = 5;
  printf("%d\n", FAV_NUM);
  //  num = 8;
  printf("%d\n", FAV_NUM);

  printf("%d", 90);


  /*
  //This prints out text.
  printf("Comments are fun.");
  */
  
  printf("\n");
  return 0;
};

/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
