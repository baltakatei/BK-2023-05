/* Desc:
 * Usage: ./test
 * Ref/Attrib: [1] https://youtu.be/ix5jPkxsr7M?t=3072
 */

#include <stdio.h>
#include <stdlib.h>

int main()
{
  int age = 40;
  double gpa = 3.6;
  char grade = 'A'; // must use single-quotes
  char phrase[100] = "baltakatei";

  printf("\n");
  printf("age:%i\n", age);
  printf("gpa:%f\n", gpa); // %f indicates float or double?
  printf("phrase:%s\n", phrase);
  printf("\n");
  
  return 0;
};

/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
