<TeXmacs|2.1.1>

<style|<tuple|book|style-bk>>

<\body>
  <doc-data|<doc-title|<TeXmacs> notes for <name|The C Programming
  Language>>|<doc-author|<author-data|<author-name|Steven William
  \PBaltakatei\Q Sandoval>|<author-email|baltakatei@gmail.com>|<author-homepage|reboil.com>>>|<doc-date|2023>>

  <include|ch1.tm>

  <include|ch2.tm>

  <include|ch3.tm>

  \;
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
    <associate|preamble|false>
    <associate|project-flag|true>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|5|ch1.tm>>
    <associate|auto-2|<tuple|1.1|5|ch1.tm>>
    <associate|auto-3|<tuple|1.2|8|ch1.tm>>
    <associate|auto-4|<tuple|1.2.1|8|ch1.tm>>
    <associate|auto-5|<tuple|2|9|ch2.tm>>
    <associate|auto-6|<tuple|2.1|9|ch2.tm>>
    <associate|auto-7|<tuple|2.2|9|ch2.tm>>
    <associate|auto-8|<tuple|3|11|ch3.tm>>
    <associate|footnote-1.1.1|<tuple|1.1.1|5|ch1.tm>>
    <associate|footnote-1.1.10|<tuple|1.1.10|7|ch1.tm>>
    <associate|footnote-1.1.11|<tuple|1.1.11|7|ch1.tm>>
    <associate|footnote-1.1.2|<tuple|1.1.2|5|ch1.tm>>
    <associate|footnote-1.1.3|<tuple|1.1.3|6|ch1.tm>>
    <associate|footnote-1.1.4|<tuple|1.1.4|6|ch1.tm>>
    <associate|footnote-1.1.5|<tuple|1.1.5|6|ch1.tm>>
    <associate|footnote-1.1.6|<tuple|1.1.6|6|ch1.tm>>
    <associate|footnote-1.1.7|<tuple|1.1.7|6|ch1.tm>>
    <associate|footnote-1.1.8|<tuple|1.1.8|6|ch1.tm>>
    <associate|footnote-1.1.9|<tuple|1.1.9|7|ch1.tm>>
    <associate|footnote-1.2.1|<tuple|1.2.1|8|ch1.tm>>
    <associate|footnote-1.2.2|<tuple|1.2.2|8|ch1.tm>>
    <associate|footnote-1.2.3|<tuple|1.2.3|8|ch1.tm>>
    <associate|footnote-1.2.4|<tuple|1.2.4|8|ch1.tm>>
    <associate|footnote-1.2.5|<tuple|1.2.5|8|ch1.tm>>
    <associate|footnr-1.1.1|<tuple|1.1.1|5|ch1.tm>>
    <associate|footnr-1.1.10|<tuple|1.1.10|7|ch1.tm>>
    <associate|footnr-1.1.11|<tuple|1.1.11|7|ch1.tm>>
    <associate|footnr-1.1.2|<tuple|1.1.2|5|ch1.tm>>
    <associate|footnr-1.1.3|<tuple|1.1.3|6|ch1.tm>>
    <associate|footnr-1.1.4|<tuple|1.1.4|6|ch1.tm>>
    <associate|footnr-1.1.5|<tuple|1.1.5|6|ch1.tm>>
    <associate|footnr-1.1.6|<tuple|1.1.6|6|ch1.tm>>
    <associate|footnr-1.1.7|<tuple|1.1.7|6|ch1.tm>>
    <associate|footnr-1.1.8|<tuple|1.1.8|6|ch1.tm>>
    <associate|footnr-1.1.9|<tuple|1.1.9|7|ch1.tm>>
    <associate|footnr-1.2.1|<tuple|1.2.1|8|ch1.tm>>
    <associate|footnr-1.2.2|<tuple|1.2.2|8|ch1.tm>>
    <associate|footnr-1.2.3|<tuple|1.2.3|8|ch1.tm>>
    <associate|footnr-1.2.4|<tuple|1.2.4|8|ch1.tm>>
    <associate|footnr-1.2.5|<tuple|1.2.5|8|ch1.tm>>
    <associate|func getchar|<tuple|1.2.1|8|ch1.tm>>
    <associate|func putchar|<tuple|3|8|ch1.tm>>
    <associate|part:ch1.tm|<tuple|?|5|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|part:ch2.tm|<tuple|1.2.5|9|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|part:ch3.tm|<tuple|2.2|11|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|ref includehelp-oct-hex|<tuple|1.1.3|6|ch1.tm>>
    <associate|term declaration|<tuple|call|5|ch1.tm>>
    <associate|term definition|<tuple|1.1.1|5|ch1.tm>>
    <associate|term enumeration|<tuple|linter|5|ch1.tm>>
    <associate|term escape_sequence|<tuple|enumeration|5|ch1.tm>>
    <associate|term garbage_collection|<tuple|1.1.10|7|ch1.tm>>
    <associate|term heap|<tuple|garbage collection|7|ch1.tm>>
    <associate|term machine-independent|<tuple|<label|term
    heap>heap|7|ch1.tm>>
    <associate|term operand|<tuple|<label|term
    machine-independent>machine-independent|7|ch1.tm>>
    <associate|term operator|<tuple|<label|term operand>operand|7|ch1.tm>>
    <associate|term stack|<tuple|parameter|7|ch1.tm>>
    <associate|term struct|<tuple|1.1.11|7|ch1.tm>>
    <associate|term structure_assignment|<tuple|<label|term
    struct>struct|7|ch1.tm>>
    <associate|term text_stream|<tuple|structure assignment|7|ch1.tm>>
    <associate|term_argument|<tuple|1.1|5|ch1.tm>>
    <associate|term_call|<tuple|assignment|5|ch1.tm>>
    <associate|term_expression|<tuple|1.1.4|6|ch1.tm>>
    <associate|term_function|<tuple|1.1.8|7|ch1.tm>>
    <associate|term_function_call|<tuple|function|7|ch1.tm>>
    <associate|term_function_prototype|<tuple|function call|7|ch1.tm>>
    <associate|term_linter|<tuple|1.1.2|5|ch1.tm>>
    <associate|term_parameter|<tuple|<label|term operator>operator|7|ch1.tm>>
    <associate|term_statement|<tuple|<label|term stack>stack|7|ch1.tm>>
    <associate|term_type|<tuple|<label|term text_stream>text
    stream|8|ch1.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|parts>
      <tuple|ch1.tm|chapter-nr|0|section-nr|0|subsection-nr|0>

      <tuple|ch2.tm|chapter-nr|1|section-nr|2|subsection-nr|1>

      <tuple|ch3.tm|chapter-nr|2|section-nr|2|subsection-nr|0>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Definitions>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      1.1<space|2spc>Terminology <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>

      1.2<space|2spc>Functions <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>

      <with|par-left|<quote|1tab>|1.2.1<space|2spc>Character Input and Output
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>Utilities>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5><vspace|0.5fn>

      2.1<space|2spc>Text Editor <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6>

      2.2<space|2spc>Linter <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|3<space|2spc>K&R
      Exercises> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-8><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>