<TeXmacs|2.1.2>

<style|source>

<\body>
  <\active*>
    <\src-title>
      <src-package|pubkeys-book-style|0.0.1>

      <src-purpose|Style package for Notable Public Keys TeXmacs source
      files.>
    </src-title>
  </active*>

  <use-package|number-long-article|libertine-font>

  <\active*>
    <\src-comment>
      Style parameters.
    </src-comment>
  </active*>

  <assign|info-flag|detailed>

  <assign|page-height|auto>

  <assign|page-width|auto>

  <assign|page-type|a4>

  <\active*>
    <\src-comment>
      Macro definitions.
    </src-comment>
  </active*>

  \;

  <assign|bktable3|<macro|body|<tformat|<twith|table-min-rows|2>|<twith|table-min-cols|2>|<cwith|1|1|1|-1|cell-tborder|2ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|-1|-1|1|-1|cell-bborder|2ln>|<cwith|1|-1|1|-1|cell-bsep|0.25fn>|<cwith|1|-1|1|-1|cell-tsep|0.25fn>|<cwith|1|-1|1|-1|cell-lsep|0.50fn>|<cwith|1|-1|1|-1|cell-rsep|0.50fn>|<arg|body>>>>

  <assign|def|<macro|body|<hgroup|<em|<inactive|<reference|<arg|body>>>>>>>

  <assign|hlinkv-delme|<macro|body|<hlink|<verbatim|<arg|body>>
  |<arg|body>>>>

  <assign|hlinkv|<macro|body|<hlink|<verbatim|<condensed|<arg|body>>>
  |<arg|body>>>>

  <assign|long-id-spc|<macro|body|<with|hmagnified-factor|0.75|color|brown|<hgroup|<hmagnified|<verbatim|<arg|body>>>>>>>

  <assign|long-id|<macro|body|<with|color|brown|<hgroup|<verbatim|0x<arg|body>>>>>>

  <assign|verbatim-8pt|<\macro|body>
    <\with|font-base-size|8>
      <\verbatim>
        <arg|body>
      </verbatim>
    </with>
  </macro>>

  <assign|verb-sm|<macro|body|<with|font-base-size|8|<verbatim|<arg|body>>>>>

  <assign|version|<macro|body|<em|v<arg|body>>>>

  <assign|vpk|<\macro|body>
    <\vgroup>
      <\verbatim-8pt>
        <\with|font-base-size|8>
          <arg|body>
        </with>
      </verbatim-8pt>
    </vgroup>

    \;
  </macro>>

  <\active*>
    <\src-comment>
      Macro definitions: Monospace code blocks
    </src-comment>
  </active*>

  <assign|code-wide|<\macro|body>
    <\wide-block>
      <tformat|<cwith|1|1|1|1|cell-background|pastel
      cyan>|<table|<row|<\cell>
        <\shell-code>
          <arg|body>
        </shell-code>
      </cell>>>>
    </wide-block>
  </macro>>

  <assign|code-wide-sm|<\macro|body>
    <with|font|Linux Libertine|font-base-size|8|<\code-wide>
      <arg|body>
    </code-wide>>
  </macro>>

  <assign|code-inline|<macro|body|<tabular|<tformat|<cwith|1|1|1|1|cell-background|pastel
  cyan>|<cwith|1|1|1|1|cell-tborder|1ln>|<cwith|1|1|1|1|cell-bborder|1ln>|<cwith|1|1|1|1|cell-lborder|1ln>|<cwith|1|1|1|1|cell-rborder|1ln>|<table|<row|<cell|<verbatim|<arg|body>>>>>>>>>
</body>

<\initial>
  <\collection>
    <associate|preamble|true>
  </collection>
</initial>