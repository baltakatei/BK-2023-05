<TeXmacs|2.1.1>

<project|book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <chapter|K&R Notes>

  <section|Ch. 1: A Tutorial Introduction>

  <subsection|Sec. 1.1: Getting Started>

  \;

  <\description>
    <item*|Exercise 1-1>Run the \P<verbatim|hello, world>\Q program on your
    system. Experiment with leaving out parts of the program, to see what
    error messages you get.

    <item*|Exercise 1-2>Experiment to find out what happens when
    <cpp|printf>'s argument string contains <cpp|\\><em|c>, where <em|c> is
    some character not listed above.
  </description>

  <subsection|Sec. 1.2: Variables and Arithmetic Expressions>

  <\description>
    <item*|Exercise 1-3>Modify the temperature conversion program to bring a
    heading above the table.

    <item*|Exercise 1-4>Write a program to bring the corresponding Celsius to
    Fahrenheit table.
  </description>

  <subsection|Sec. 1.3: The For Statement>

  <\description>
    <item*|Exercise 1-5>Modify the temperature conversion program to print
    the table in reverse order, that is, from <math|300> degrees to <math|0>.
  </description>

  <subsection|Sec. 1.4: Symbolic Constants>

  <subsection|Sec. 1.5: Character Input and Output>

  <subsubsection|Sec. 1.5.1: File Copying>

  <\description>
    <item*|Exercise 1-6>Verify that the expression <cpp|getchar() != EOF> is
    <math|0> or <math|1>.

    <item*|Exercise 1-7>Write a program to print the value of <cpp|EOF>.
  </description>

  <subsubsection|Sec. 1.5.2: Character Counting>

  <subsubsection|Sec. 1.5.3: Line Counting>

  <\description>
    <item*|Exercise 1-8>Write a program to count blanks, tabs, and newlines.

    <item*|Exercise 1-9>Write a program to copy its input to its output,
    replacing each string of one or more blanks by a single blank.

    <item*|Exercise 1-10>Write a program to copy its input to its output,
    replacing each tab by <cpp|\\t>, each backspace by <cpp|\\b>, and each
    backslash by <cpp|\\\\>. This makes tabs and backspaces visible in an
    unambiguous way.
  </description>

  <subsubsection|Sec. 1.5.4: Word Counting>

  <\description>
    <item*|Exercise 1-11>How would you test the word program? What kinds of
    input are most likely to uncover bugs if there are any?

    <item*|Exercise 1-12>Write a program that prints its input one word per
    line.
  </description>

  <subsection|Sec. 1.6: Arrays>

  <\description>
    <item*|Exercise 1-13>Write a program to print a histogram of the lengths
    of words in its input. IT is easy to draw the histogram with the bars
    horizontal; a vertical orientation is more challenging.

    <item*|Exercise 1-14>Write a program to print a histogram of the
    frequencies of different characters in its input.
  </description>

  <subsection|Sec 1.7: Functions>

  <\description>
    <item*|Exercise 1-15>Rewrite the temperature conversion program of
    Section 1.2 to use a function for conversion.
  </description>

  <subsection|Sec 1.8: Arguments\UCall by Value>

  <subsection|Sec 1.9: Character Arrays>

  <\description>
    <item*|Exercise 1-16>Revise the main routine of the longest-line program
    so it will correctly print the length of arbitrarily long input lines,
    and as much as possible of the text.

    <item*|Exercise 1-17>Write a program to print all input lines that are
    longer than <math|80> characters.

    <item*|Exercise 1-18>Write a program to remove trailing blanks and tabs
    from each line of input, and to delete entirely blank lines.

    <item*|Exercise 1-19>Write a function <cpp|reverse(s)> that reverses the
    character string <cpp|s>. Use it to write a program that reverses its
    input a line at a time.
  </description>

  <subsection|Sec 1.10: External Variables and Scope>

  <\description>
    <item*|Exercise 1-20>Write a program <cpp|detab> that replaces tabs in
    the input with the proper number of blanks to space to the next tab stop.
    Assume a fixed set of tab stops, say every <em|n> columns. Should <em|n>
    be a variable or a symbolic parameter?

    <item*|Exercise 1-21>Write a program <cpp|entab> that replaces strings of
    blanks by the minimum number of tabs and blanks to achieve the same
    spacing. Use the same tab stops as for <cpp|detab>. When either a tab or
    a single blank would suffice to each a tab stop, which should be given
    preference?

    <item*|Exercise 1-22>Write a program to \Pfold\Q long input lines into
    two or more shorter lines after the last non-blank character that occurs
    before the <em|n>-th column of input. Make sure your program does
    something intelligent with very long lines, and if there are no blanks or
    tabs before the specified column.

    <item*|Exercise 1-23>Write a program to remove all comments from a
    <name|C> program. Don't forget to handle quoted strings and character
    constants properly. <name|C> comments do not nest.

    <item*|Exercise 1-24>Write a program to check a <name|C> program for
    rudimentary syntax errors like unbalanced parentheses, brackets and
    braces. Don't forget about quotes, both single and double, escape
    sequences, and comments. (This program is hard if you do it in full
    generality.)
  </description>

  <section|Ch2: Types, Operators, and Expressions>

  <subsection|Sec 2.1: Variable names>

  <subsection|Sec 2.2: Data Types and Sizes>

  <\description>
    <item*|Exercise 2-1>Write a program to determine the ranges of
    <cpp|char>, <cpp|short>, <cpp|int>, and <cpp|long> variables, both
    <cpp|signed> and <cpp|unsigned>, by printing appropriate values from
    standard headers and by direct computation. Harder if you compute them:
    determine the ranges of the various floating-point types.
  </description>

  <subsection|Sec 2.3: Constants>

  <subsection|Sec 2.4: Declarations>

  <subsection|Sec 2.5: Arithmetic Operators>

  <subsection|Sec 2.6: Relational and Logical Operators>

  <\description>
    <item*|Exercise 2-2>Write a loop equivalent to the <cpp|for> loop above
    without using <cpp|&&> or <cpp|\|\|>.
  </description>

  <subsection|Sec 2.7: Type Conversions>

  <\description>
    <item*|Exercise 2-3>Write the function <cpp|htoi(s)>, which converts a
    string of hexadecimal digits (including an optional <cpp|0x> or <cpp|0X>)
    into its equivalent integer value. The allowable digits are <cpp|0>
    through <cpp|9>, <cpp|a> through <cpp|f>, and <cpp|A> through <cpp|F>.
  </description>

  <subsection|Sec 2.8: Increment and Decrement Operators>

  <\description>
    <item*|Exercise 2-4>Write an alternate version of <cpp|squeeze(s1,s2)>
    that deletes each character in <cpp|s1> that matches any character in the
    <em|string> <cpp|s2>.

    <item*|Exercise 2-5>Write the function <cpp|any(s1,s2)>, which returns
    the first location in the string <cpp|s1> where any character fromt he
    string <cpp|s2> occurs, or <cpp|-1> if <cpp|s1> contains no characters
    from <cpp|s2>. (The standard library function <cpp|strpbrk> does the same
    job but returns a pointer to the location.)
  </description>

  <subsection|Sec 2.9: Bitwise Operators>

  <\description>
    <item*|Exercise 2-6>Write a function <cpp|setbits(x,p,n,y)> that returns
    <cpp|x> with the <cpp|n> bits that begin at position <cpp|p> set to the
    rightmost <cpp|n> bits of <cpp|y>, leaving the other bits unchanged.

    <item*|Exercise 2-7>Write a function <cpp|invert(x,p,n)> that returns
    <cpp|x> with the <cpp|n> bits that begin at position <cpp|p> inverted
    (i.e. <math|1> changed into <math|0> and vice versa), leaving the others
    unchanged.

    <item*|Exercise 2-8>Write a function <cpp|rightrot(x,n)> that returns the
    value of the integer <cpp|x> rotated to the right by <cpp|n> bit
    positions.
  </description>

  <subsection|Sec 2.10: Assignment Operators and Expressions>

  <\description>
    <item*|Exercise 2-9>In a two\<#2019\>s complement number system, <cpp|x
    &= (x-1)> deletes the rightmost <math|1>-bit in <cpp|x>. Explain why. Use
    this observation to write a faster version of <cpp|bitcount>.
  </description>

  <subsection|Sec 2.11: Conditional Expressions>

  <\description>
    <item*|Exercise 2-10>Rewrite the function <cpp|lower>, which converts
    upper case letters to lower case, with a conditional expression instead
    of <cpp|if-else>.
  </description>

  <subsection|Sec 2.12: Precedence and Order of Evaluation>

  <section|Ch3: Control Flow>

  <subsection|Sec 3.1: Statements and Blocks>

  <subsection|Sec 3.2: If-Else>

  <subsection|Sec 3.3: Else-If>

  <\description>
    <item*|Exercise 3-1>Our binary search makes two tests inside th eloop,
    when one would suffice (at the price of more tests outside). Write a
    version with only one test inside the loop and measure the difference in
    run-time.
  </description>

  <subsection|Sec 3.4: Switch>

  <\description>
    <item*|Exercise 3-2>Write a function <cpp|escape(s,t)> that converts
    characters like newline and tab into visible escape sequences like
    <cpp|\\n> and <cpp|\\t> as it copies the string <cpp|t> to <cpp|s>. Use a
    <cpp|switch>. Write a function for the other direction as well,
    converting escape sequences into the real characters.
  </description>

  <subsection|Sec 3.5: Loops>

  <\description>
    <item*|Exercise 3-3>Write a function <cpp|expand(s1,s2)> that expands
    shorthand notations like <cpp|a-z> in the string <cpp|s1> into the
    equivalent complete list <cpp|abc...xyz> in <cpp|s2>. Allow for letters
    of either case and digits, and be prepared to handle cases like
    <cpp|a-b-c> and <cpp|a-z0-9> and <cpp|-a-z>. Arrange that a leading or
    trailing <cpp|-> is taken literally.
  </description>

  <\subsection>
    Sec 3.6: Loops\UDo-while
  </subsection>

  <\description>
    <item*|Exercise 3-4>In a two's complement number representation, our
    version of <cpp|itoa> does not handle the largest negative number, that
    is, the value of <cpp|n> equal to <math|-<around*|(|2<rsup|<text|wordsize>-1>|)>>.
    Explain why not. Modify it to print that value correctly, regardless of
    the machine on which it runs.

    <item*|Exercise 3-5>Write the function <cpp|itob(n,s,b)> that converts
    the integer <cpp|n> into a base <cpp|b> character representation in the
    string <cpp|s>. In particular, <cpp|itob(n,s,16)> formats <cpp|n> as a
    hexadecimal integer in <cpp|s>.

    <item*|Exercise 3-6>Write a version of <cpp|itoa> that accepts three
    arguments instead of two. The third argument is a minimum field width;
    the converted number must be padded with blanks on the left if necessary
    to make it wide enough.
  </description>

  <subsection|Sec 3.7: Break and Continue>

  <subsection|Sec 3.8: Goto and Labels>

  <section|Ch 4: The C Preprocessor>

  <subsection|Sec 4.1: Basics of Functions>

  <\description>
    <item*|Exercise 4-1>Write the function <cpp|strrindex(s,t)>, which
    returns the position of the <em|rightmost> occurence of <cpp|t> in
    <cpp|s>, or <cpp|-1> if there is none.
  </description>

  <subsection|Sec 4.2: Functions Returning Non-integers>

  <\description>
    <item*|Exercise 4-2>Extend <cpp|atof> to handle scientific notation of
    the form

    <\cpp-code>
      123.45e-6
    </cpp-code>

    where a floating-point number may be followed by <cpp|e> or <cpp|E> and
    an optionally signed exponent.
  </description>

  <subsection|Sec 4.3: External Variables>

  <\description>
    <item*|Exercise 4-3>Given the basic framework, it's straightforward to
    extend the calculator. Add the modulus (<cpp|%>) operator and provisions
    for negative numbers.

    <item*|Exercise 4-4>Add commands to print the top element of the stack
    without popping, to duplicate it, and to swap the top two elements. Add a
    command to clear the stack.

    <item*|Exercise 4-5>Add access to library functions like <cpp|sin>,
    <cpp|exp>, and <cpp|pow>. See <cpp|\<less\>math.h\<gtr\>> in Appendix B,
    Section 4.

    <item*|Exercise 4-6>Add commands for handling variables. (It's easy to
    provide twenty-six variables with single-letter names.) Add a variable
    for the most recently printed value.

    <item*|Exercise 4.7>Write a routine <cpp|ungets(s)> that will push back
    an entire string onto the input. Should <cpp|ungets> know about <cpp|buf>
    and <cpp|bufp>, or should it just use <cpp|ungetch>?

    <item*|Exercise 4-8>Suppose that there will never be more than one
    character of pushback. Modify <cpp|getch> and <cpp|ungetch> accordingly.

    <item*|Exercise 4-9>Our <cpp|getch> and <cpp|ungetch> do not handle a
    pushed-back <cpp|EOF> correctly. Decide what their properties ought to be
    if an <cpp|EOF> is pushed back, then implement your design.

    <item*|Exercise 4-10>An alternate organization uses <cpp|getline> to read
    an entire input line; this makes <cpp|getch> andj <cpp|ungetch>
    unnecessary. Revise the calculator to use this approach.
  </description>

  <subsection|Sec 4.4: Scope Rules>

  <subsection|Sec 4.5: Header Files>

  <subsection|Sec 4.6: Static Variables>

  <subsection|Sec 4.7: Register Variables>

  <subsection|Sec 4.8: Block Structure>

  <subsection|Sec 4.9: Initialization>

  <subsection|Sec 4.10: Recursion>

  <\description>
    <item*|Exercise 4-12>Adapt the ideas of <cpp|printd> to write a recursive
    version of <cpp|itoa>; that is, convert an integer into a string by
    calling a recursive routine.

    <item*|Exercise 4-13>Write a recursive version of the function
    <cpp|reverse(s)>, which reverses the string <cpp|s> in place.
  </description>

  <subsection|Sec 4.11: The C Preprocessor>

  <subsubsection|Sec 4.11.1: File Inclusion>

  <subsubsection|Sec 4.11.2: Macro Substitution>

  <\description>
    <item*|Exercise 4-14>Define a macro <cpp|swap(t,x,y)> that interchanges
    two arguments of type <cpp|t>. (Block structure will help.)
  </description>

  <subsubsection|Sec 4.11.3: Conditional Inclusion>

  <section|Ch 5: Pointers and Arrays>

  <subsection|Sec 5.1: Pointers and Addresses>

  <subsection|Sec 5.2: Pointers and Function Arguments>

  <\description>
    <item*|Exercise 5-1>As written, <cpp|getint> treats a <cpp|+> or <cpp|->
    not followed by a digit as a valid representation of zero. Fix it to push
    such a character back onto the input.

    <item*|Exercise 5-2>Write <cpp|getfloat>, the floating-point analog of
    <cpp|getint>. What type does <cpp|getfloat> return as its function value?
  </description>

  <subsection|Sec 5.3: Pointers and Arrays>

  <subsection|Sec 5.4: Address Arithmetic>

  <subsection|Sec 5.5: Character Pointers and Functions>

  <\description>
    <item*|Exercise 5-3>Write a pointer version of the function <cpp|strcat>
    that we showed in Chapter 2: <cpp|strcat(s,t)> copies the string <cpp|t>
    to the end of <cpp|s>.

    <item*|Exercise 5-4>Write the function <cpp|strend(s,t)>, which returns
    <cpp|1> if the string <cpp|t> occurs at the end of the string <cpp|s>,
    and zero otherwise.

    <item*|Exercise 5-5>Write versions of the library functions
    <cpp|strncpy>, <cpp|strncat>, and <cpp|strncmp>, which operate on at most
    the first <cpp|n> characters of their argument strings. For example,
    <cpp|strncpy(s,t,n)> copies at most <cpp|n> characters of <cpp|t> to
    <cpp|s>. Full descriptions are in Appendix B.

    <item*|Exercise 5-6>Rewrite appropriate programs from earlier chapters
    and exercises with pointers instead of array indexing. Good possibilities
    include <cpp|getline> (Chapters 1 and 4), <cpp|atoi>, <cpp|itoa>, and
    their variants (Chapters 2, 3, and 4), <cpp|reverse> (Chapter 3), and
    <cpp|strindex> and <cpp|getop> (Chapter 4).
  </description>

  <subsection|Sec 5.6: Pointer Arrays; Pointers to Pointers>

  <\description>
    <item*|Exercise 5-7>Rewrite <cpp|readlines> to store lines in an array
    supplied by <cpp|main>, rather than calling <cpp|alloc> to maintain
    storage. How much faster is the program?
  </description>

  <subsection|Sec 5.7: Multi-dimensional Arrays>

  <\description>
    <item*|Exercise 5-8>There is no error checking in <cpp|day_of_year> or
    <cpp|month_day>. Remedy this defect.
  </description>

  <subsection|Sec 5.8: Initialization of Pointer Arrays>

  <subsection|Sec 5.9: Pointers vs. Multi-dimensional Arrays>

  <\description>
    <item*|Exercise 5-9>Rewrite the routines <cpp|day_of_year> and
    <cpp|month_day> with pointers instead of reindexing.
  </description>

  <subsection|Sec 5.10: Command-line Arguments>

  \;

  <\description>
    <item*|Exercise 5-10>Write the program <cpp|expr>, which evaluates a
    reverse Polish expression fromt he command line, where each operator or
    operand is a separate argument. For example,

    <\cpp-code>
      expr \ 2 \ 3 \ 4 \ + \ *
    </cpp-code>

    evaluates <math|2\<times\><around*|(|3+4|)>>.

    <item*|Exercise 5-11>Modify the programs <cpp|entab> and <cpp|detab>
    (written as exercises in Chapter 1) to accept a list of tab stops as
    arguments. Use the default tab settings if there are no arguments.

    <item*|Exercise 5-12>Extend <cpp|entab> and <cpp|detab> to accept the
    shorthand

    <\cpp-code>
      entab -<text|<em|m>> +<text|<em|n>>
    </cpp-code>

    to mean tab stops every <em|n> columns, starting at column <em|m>. Choose
    convenient (for the user) default behavior.

    <item*|Exercise 5-13>Write the program <cpp|tail>, which prints the last
    <em|n> lines of its input. By default, <em|n> is <math|10>, let us say,
    but it canb e changed by an optional argument so that

    <\cpp-code>
      tail -<text|<em|n>>
    </cpp-code>

    prints the last <em|n> lines. The program should behave rationally no
    matter how unreasonable the input or the value of <em|n>. Write the
    program so it makes the best use of available storage; lines should be
    stored as in the sorting program of Section 5.6, not in a two-dimensional
    array of fixed size.
  </description>

  <subsection|Sec 5.11: Pointers to Functions>

  <\description>
    <item*|Exercise 5-14>Modify the sort program to handle a <cpp|-r> flag,
    which indicates sorting in reverse (decreasing) order. Be sure that
    <cpp|-r> works with <cpp|-n>.

    <item*|Exercise 5-15>Add the option <cpp|-f> to fold upper and lower case
    together, so that case distinctions are not made during sorting; for
    example, <cpp|a> and <cpp|A> compare equal.

    <item*|Exercise 5-16>Add the <cpp|-d> (\Pdirectory\Q) option, which makes
    comparisons only on letters, numbers, and blanks. Make sure it works in
    conjunction with <cpp|-f>.

    <item*|Exercise 5-17>Add a field-handling capability, so sorting may be
    done on fields within lines, each field sorted according to an
    independent set of options. (The index for this book was sorted with
    <cpp|-df> for the index category and <cpp|-n> for the page numbers.)
  </description>

  <subsection|Sec 5.12: Complicated Declarations>

  <\description>
    <item*|Exercise 5-18>Make <cpp|dcl> recover from input errors.

    <item*|Exercise 5-19>Modify <cpp|undcl> so that it does not add redundant
    parentheses to declarations.

    <item*|Exercise 5-20>Expand <cpp|dcl> to handle declarations with
    function argument types, qualifiers like <cpp|const>, and so on.
  </description>

  <section|Ch 6: Structures>

  <subsection|Sec 6.1: Basics of Structures>

  <subsection|Sec 6.2: Structures and Functions>

  <subsection|Sec 6.3: Arrays of Structures>

  <\description>
    <item*|Exercise 6-1>Our version of <cpp|getword> does not properly handle
    underscores, string constants, comments, or preprocessor control lines.
    Write a better version.
  </description>

  <subsection|Sec 6.4: Pointers to Structures>

  <subsection|Sec 6.5: Self-referential Structures>

  <\description>
    <item*|Exercise 6-2>Write a program that reads a <name|C> program and
    prints in alphabetical order each group of variable names that are
    identical in the first <math|6> characters, but different somewhere
    thereafter. Don't count words within strings and comments. Make <math|6>
    a parameter that can be set from the command line.

    <item*|Exercise 6-3>Write a cross-referencer that prints a list of all
    words in a document, and, for each word, a lis tof the line numbers on
    which it occurs. Remove noise words like \Pthe\Q, \Pand\Q, and so on.

    <item*|Exercise 6-4>Write a program that prints the distinct words in its
    input sorted into decreasing order of frequency of occurrence. Precede
    each word by its count.
  </description>

  <subsection|Sec 6.6: Table Lookup>

  <\description>
    <item*|Exercise 6-5>Write a function <cpp|undef> that will remove a name
    and definition from the table maintained by <cpp|lookup> and
    <cpp|install>.

    <item*|Exercise 6-6>Implement a simple version of the <cpp|#define>
    processor (i.e., no arguments) suitable for use with <name|C> programs,
    based on the routines of this section. You may also find <cpp|getch> and
    <cpp|ungetch> helpful.
  </description>

  <subsection|Sec 6.7: Typedef>

  <subsection|Sec 6.8: Unions>

  <subsection|Sec 6.9: Bit-fields>

  <section|Ch 7: Input and Output>

  <subsection|Sec 7.1: Standard Input and Output>

  <\description>
    <item*|Exercise 7-1>Write a program that converts upper case to lower or
    lower case to upper, depending on the name it is invoked with, as found
    in <cpp|argv[0]>.
  </description>

  <subsection|Sec 7.2: Formatted Output\UPrintf>

  <\description>
    <item*|Exercise 7-2>Write a program that will print arbitrary input in a
    sensible way. AS a minimum, it ishould print non-graphic characters in
    octal or hexadecimal according to local custom, and break long text
    lines.
  </description>

  <subsection|Sec 7.3: Variable-length Argument Lists>

  <\description>
    <item*|Exercise 7-3>Revise <cpp|minprintf> to handle more of the other
    facilities of <cpp|printf>.
  </description>

  <subsection|Sec 7.4: Formatted Input\UScanf>

  <\description>
    <item*|Exercise 7-4>Write a private version of <cpp|scanf> analogous to
    <cpp|minprintf> from the previous section.

    <item*|Exercise 7-5>Rewrite the postfix calculator of Chapter 4 to use
    <cpp|scanf> and/or <cpp|sscanf> to do the input and number conversion.
  </description>

  <subsection|Sec 7.5: File Access>

  <subsection|Sec 7.6: Error Handling\UStderr and Exit>

  <subsection|Sec 7.7: Line Input and Output>

  <\description>
    <item*|Exercise 7-6>Write a program to compare two files, printing the
    first line where they differ.

    <item*|Exercise 7-7>Modify the pattern finding program of Chapter 5 to
    take its input from a set of named files or, if no files are named as
    arguments, from the standard input. Should the file name be printed when
    a matching line is found?

    <item*|Exercise 7-8>Write a program to print a set of files, starting
    each new one on a new page, with a title and a running page count for
    each file.
  </description>

  <subsection|Sec 7.8: Miscellaneous Functions>

  <subsubsection|Sec 7.8.1: String Operations>

  <subsubsection|Sec 7.8.2: Character Class Testing and Conversion>

  <subsubsection|Sec 7.8.3: Ungetc>

  <subsubsection|Sec 7.8.4: Command Execution>

  <subsubsection|Sec 7.8.5: Storage Management>

  <subsubsection|Sec 7.8.6: Mathematical Functions>

  <subsubsection|Sec 7.8.7: Random Number Generation>

  <\description>
    <item*|Exercise 7-9>Functions like <cpp|isupper> can be implemented to
    save space or to save time. Explore both possibilities.
  </description>

  <section|Ch 8: The UNIX System Interface>

  <subsection|Sec 8.1: File Descriptors>

  <subsection|Sec 8.2: Low Level I/O\URead and Write>

  <subsection|Sec 8.3: Open, Creat, Close, Unlink>

  <\description>
    <item*|Exercise 8-1>Rewrite the program <cpp|cat> from Chapter 7 using
    <cpp|read>, <cpp|write>, <cpp|open>, and <cpp|close> instead of their
    standard library equivalents. Perform experiemnts to determine the
    relative speeds of the two versions.
  </description>

  <subsection|Sec 8.4: Random Access\ULseek>

  <subsection|Sec 8.5: Example\UAn Implementation of Fopen and Getc>

  <\description>
    <item*|Exercise 8-2>Rewrite <cpp|fopen> and <cpp|_fillbuf> with fields
    instead of explicit bit operations. Compare code size and execution
    speed.

    <item*|Exercise 8-3>Design and write <cpp|_flushbuf>, <cpp|fflush>, and
    <cpp|fclose>.

    <item*|Exercise 8-4>The standard library function

    <\cpp-code>
      int fseek(FILE *fp, long offset, int origin)
    </cpp-code>

    is identical to <cpp|lseek> except that <cpp|fp> is a file pointer
    instead of a file descriptor and the return value is an <cpp|int> status,
    not a position. Write <cpp|fseek>. Make sure that your <cpp|fseek>
    coordinates properly with the buffering done for the other functions of
    the library.
  </description>

  <subsection|Sec 8.6: Example\UListing Directories>

  <\description>
    <item*|Exercise 8-5>Modify the <cpp|fsize> program to print the other
    information contained in the inode entry.
  </description>

  <subsection|Sec 8.7: Example\UA Storage Allocator>

  <\description>
    <item*|Exercise 8-6>The standard library function <cpp|calloc(n,size)>
    returns a pointer to <cpp|n> objects of size <cpp|size>, with the storage
    initialized to zero. Write <cpp|calloc>, by calling <cpp|malloc> or by
    modifying it.

    <item*|Exercise 8-7><cpp|malloc> accepts a size request without checking
    its plausibility; <cpp|free> believes that the block it is asked to free
    contains a valid size field. Improve these routins so they take more
    pains with error checking.

    <item*|Exercise 8-8>Write a routine <cpp|bfree(p,n)> that will free an
    arbitrary block <cpp|p> of <cpp|n> characters into the free list
    maintained by <cpp|malloc> and <cpp|free>. By using <cpp|bfree>, a user
    can add a static or external array to the free list at any time.
  </description>

  \;

  \;
</body>

<\initial>
  <\collection>
    <associate|page-medium|papyrus>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-10|<tuple|1.1.5.3|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-100|<tuple|1.8.7|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-11|<tuple|1.1.5.4|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-12|<tuple|1.1.6|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-13|<tuple|1.1.7|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-14|<tuple|1.1.8|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-15|<tuple|1.1.9|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-16|<tuple|1.1.10|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-17|<tuple|1.2|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-18|<tuple|1.2.1|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-19|<tuple|1.2.2|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-2|<tuple|1.1|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-20|<tuple|1.2.3|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-21|<tuple|1.2.4|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-22|<tuple|1.2.5|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-23|<tuple|1.2.6|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-24|<tuple|1.2.7|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-25|<tuple|1.2.8|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-26|<tuple|1.2.9|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-27|<tuple|1.2.10|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-28|<tuple|1.2.11|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-29|<tuple|1.2.12|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-3|<tuple|1.1.1|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-30|<tuple|1.3|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-31|<tuple|1.3.1|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-32|<tuple|1.3.2|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-33|<tuple|1.3.3|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-34|<tuple|1.3.4|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-35|<tuple|1.3.5|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-36|<tuple|1.3.6|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-37|<tuple|1.3.7|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-38|<tuple|1.3.8|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-39|<tuple|1.4|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-4|<tuple|1.1.2|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-40|<tuple|1.4.1|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-41|<tuple|1.4.2|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-42|<tuple|1.4.3|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-43|<tuple|1.4.4|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-44|<tuple|1.4.5|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-45|<tuple|1.4.6|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-46|<tuple|1.4.7|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-47|<tuple|1.4.8|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-48|<tuple|1.4.9|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-49|<tuple|1.4.10|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-5|<tuple|1.1.3|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-50|<tuple|1.4.11|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-51|<tuple|1.4.11.1|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-52|<tuple|1.4.11.2|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-53|<tuple|1.4.11.3|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-54|<tuple|1.5|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-55|<tuple|1.5.1|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-56|<tuple|1.5.2|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-57|<tuple|1.5.3|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-58|<tuple|1.5.4|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-59|<tuple|1.5.5|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-6|<tuple|1.1.4|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-60|<tuple|1.5.6|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-61|<tuple|1.5.7|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-62|<tuple|1.5.8|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-63|<tuple|1.5.9|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-64|<tuple|1.5.10|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-65|<tuple|1.5.11|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-66|<tuple|1.5.12|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-67|<tuple|1.6|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-68|<tuple|1.6.1|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-69|<tuple|1.6.2|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-7|<tuple|1.1.5|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-70|<tuple|1.6.3|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-71|<tuple|1.6.4|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-72|<tuple|1.6.5|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-73|<tuple|1.6.6|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-74|<tuple|1.6.7|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-75|<tuple|1.6.8|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-76|<tuple|1.6.9|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-77|<tuple|1.7|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-78|<tuple|1.7.1|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-79|<tuple|1.7.2|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-8|<tuple|1.1.5.1|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-80|<tuple|1.7.3|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-81|<tuple|1.7.4|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-82|<tuple|1.7.5|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-83|<tuple|1.7.6|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-84|<tuple|1.7.7|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-85|<tuple|1.7.8|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-86|<tuple|1.7.8.1|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-87|<tuple|1.7.8.2|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-88|<tuple|1.7.8.3|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-89|<tuple|1.7.8.4|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-9|<tuple|1.1.5.2|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-90|<tuple|1.7.8.5|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-91|<tuple|1.7.8.6|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-92|<tuple|1.7.8.7|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-93|<tuple|1.8|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-94|<tuple|1.8.1|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-95|<tuple|1.8.2|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-96|<tuple|1.8.3|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-97|<tuple|1.8.4|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-98|<tuple|1.8.5|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-99|<tuple|1.8.6|?|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>K&R
      Exercises> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>