<TeXmacs|2.1.1>

<project|book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <chapter|Definitions>

  <section|Terminology>

  <\description-compact>
    <item*|argument<label|term_argument>>A value used to supply a
    <strong|parameter> in the call of a <strong|function>. Also known as an
    \Pactual argument\Q as opposed to \Pformal argument\Q (i.e.
    <strong|parameter>). See also <em|parameter>.

    <item*|assignment>An expression in which a variable is set to a value.
    For example, in the expression <hgroup|<cpp|x = 1>>, the variable <cpp|x>
    is set to the value <cpp|1> because <cpp|x> is to the left of the equals
    sign \ \P<cpp|=>\Q. The value of the entire expression is equal to the
    value of the left hand side (i.e. <cpp|x> in this example) after the
    assignment is performed. For example, the following C code will print
    \P<cpp|true>\Q:

    <\cpp-code>
      #include \<less\>stdio.h\<gtr\>

      int main() {

      \ \ int c;

      \ \ if( (c = 7) == 7 )

      \ \ \ \ printf("true");

      \ \ else

      \ \ \ \ printf("false");

      };
    </cpp-code>

    As another example, the following lines are equivalent:

    <\cpp-code>
      a = b = c = 0;

      a = ( b = ( c = 0 ) );
    </cpp-code>

    <label|term_call><item*|call>The act of running (or \Pinvoking\Q) a
    <strong|function>. See <em|function call>.

    <label|term declaration><item*|declaration>A construct that establishes
    an association between a particular variable, function, or type and its
    attributes<\footnote>
      See <hlinkv|https://en.cppreference.com/w/c/language/declarations>.
    </footnote>. Announces the properties of variables. May be a statement
    consisting of a <strong|type> name followed by a list of
    <strong|variables> (e.g. <cpp|int i, j;>) or followed by an expression
    with a variable (e.g. <cpp|int i = 0;>). Compare with
    <with|font-series|bold|definition>.

    <label|term definition><item*|definition>A construct that establishes the
    same associations as a declaration but also causes storage to be
    allocated for the variable<\footnote>
      See <hlinkv|https://docs.microsoft.com/en-us/cpp/c-language/c-declarations-and-definitions?view=msvc-170>.
    </footnote>.

    <label|term_linter><item*|linter> A source code analysis program designed
    to detect common syntactic errors.

    <label|term enumeration><item*|enumeration><hlink|Enumeration|https://en.wikipedia.org/wiki/Enumeration>
    (or <code*|enum>) is a user defined data type in <name|C>. It is mainly
    used to assign names to integral constants. For example, the declaration
    <code*|enum year{Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov,
    Dec};> allows writing a for loop with names of months <code*|for (i=Jan;
    i\<less\>=Dec; i++)> to cycle <code*|i> through the integers <code*|0, 1,
    2, 3, <text-dots>, 10, 11>.

    <label|term escape_sequence><item*|escape sequence>A set of characters
    used to represent hard-to-type or invisible characters. Some commonly
    used escape sequences in <name|C> include:

    <\description-aligned>
      <item*|<cpp|\\n>>Represents the <em|newline> character.

      <item*|<cpp|\\t>>Represents the <em|tab> character.

      <item*|<cpp|\\b>>Represents the <em|backspace> character.

      <item*|<cpp|\\\\>>Represents the <em|backslash> (i.e.
      \P<verbatim|<em|\\>>\Q) character.
    </description-aligned>

    All other escape sequences used in <name|C> are:

    <\description-aligned>
      <item*|<cpp|\\a>>Represents the <em|alert> (bell) character.

      <item*|<cpp|\\f>>Represents the <em|formfeed> character.

      <item*|<cpp|\\r>>Represents the <em|carriage return> character.

      <item*|<cpp|\\t>>Represents the <em|horizontal tab> character.

      <item*|<cpp|\\v>>Represents the <em|vertical tab> character.

      <item*|<cpp|\\?>>Represents the <em|question mark> character.

      <item*|<cpp|\\'>>Represents the <em|single quote> character.

      <item*|<cpp|\\">>Represents the <em|double quote> character.

      <item*|<cpp|\\><em|ooo>>Represents an <em|octal number> (e.g.
      <cpp|\\012> is the <em|newline> character)<\footnote>
        <label|ref includehelp-oct-hex>See
        <hlinkv|https://www.includehelp.com/c/octal-and-hexadecimal-escape-sequences.aspx>.
      </footnote>.

      <item*|<cpp|\\x><em|hh>>Represents a <em|hexadecimal> number (e.g.
      <cpp|\\x0A> is the <em|newline> character)<rsup|<reference|ref
      includehelp-oct-hex>>.

      <item*|<cpp|\\0>>Represents the <em|null> character (i.e. a <cpp|char>
      with value <cpp|0>)<\footnote>
        See <hlinkv|https://www.geeksforgeeks.org/difference-between-null-pointer-null-character-0-and-0-in-c-with-examples/>.
      </footnote>
    </description-aligned>

    <label|term_expression><item*|expression> A sequence of
    <strong|operators> and <strong|operands> that specify a computation. When
    evaluated, an expression may perform useful side effects (e.g.
    <cpp|printf("%d", 4)> sends the character <cpp|4> to \ the standard
    output stream)<\footnote>
      See <hlinkv|https://en.cppreference.com/w/c/language/expressions>.
    </footnote>.

    Arrangements of expressions may include<\footnote>
      See <hlinkv|https://www.educative.io/answers/what-are-expressions-in-c>.
    </footnote>:

    <\description>
      <item*|Constant>A value with no operator is used<\footnote>
        See <hlinkv|https://www.cs.miami.edu/home/burt/learning/Math120.1/Notes/exp-syn.html>.
      </footnote>. (e.g. the \P<cpp|0>\Q in <cpp|return 0;>)

      <item*|Variable identifier>A variable evaluated earlier. (e.g. the 2nd
      \P<cpp|k>\Q in <hgroup|<cpp|k = 1; return k;>>)

      <item*|Infix expression>Operator is used between operands. (e.g. <cpp|a
      = x + y>)

      <item*|Postfix expression>Operator is used after the operands. (e.g.
      <cpp|xy+>)

      <item*|Prefix expression>Operator is used before the operands. (e.g.
      <cpp|+xy>)

      <item*|Unary expression>There is one operator and one operand. (e.g.
      <cpp|x++>)

      <item*|Binary expression>There is one operator and two operands. (e.g.
      <cpp|x+y>)
    </description>

    Types of expressions may include:

    <\description>
      <item*|Arithmetic expression>Consists of arithmetic operators (<cpp|+,
      -, *, and />) and computes values of <cpp|int, float, or double> type.

      <item*|Relational expression>Usually uses comparison operators
      (<cpp|\<gtr\>, \<less\>, \<gtr\>=, \<less\>=, ===, and !==>) and
      computers answer in the <cpp|bool> type (i.e. true \P<cpp|1>\Q or false
      \P<cpp|0>\Q).

      <item*|Logical expression>Consists of logical operators (<cpp|&&, \|\|,
      and !>) and combines relational expressions to compute answers in the
      <cpp|bool> type.

      <item*|Conditional expression>Consists of statements that return
      <cpp|true> if the condition is met or <cpp|false> otherwise.

      <item*|Pointer expression>May consist of an ampersand (<cpp|&>)
      operator and returns <cpp|address> values.

      <item*|Bitwise expression>Consists of bitwise operators
      (<cpp|\<gtr\>\<gtr\>, \<less\>\<less\>, ~, &, \|, and ^>) and performs
      operations at the bit level.

      <item*|Constant expression>Involves only constants such that the
      expression may be evaluated during compilation rather than
      run-time<\footnote>
        K&R 2nd Ed., Sec. 2.3 \PConstants\Q.
      </footnote>.
    </description>

    <label|term_function><item*|function>A group of statements that perform a
    task. Is defined using a function <strong|definition> that has a
    parenthesized list of <strong|declarations> called <strong|parameters>
    (e.g. variables for accepting values named <strong|arguments> as input
    when the <strong|function> is called). A function definition has the
    form:

    <em|<\indent>
      return-type<space|1em>function-name(parameter declarations, if any) {

      <space|1em>declarations

      <space|1em>statements

      }
    </indent>>

    <label|term_function_call><item*|function call>An expression containing
    the <strong|function> name followed by the function call operator
    \P<cpp|()>\Q.

    <label|term_function_prototype><item*|function prototype>A declaration of
    a <strong|function> consisting only of the function name (a.k.a.
    \Pidentifier\Q) and parenthesized <strong|parameter> types so the
    compiler knows how to perform <strong|type>-based conversions of
    <strong|arguments> (e.g. truncating a <cpp|float> into an <cpp|int>) in
    <strong|calls> of the function before the compiler knows the function
    <strong|definition><\footnote>
      See <hlinkv|https://en.cppreference.com/w/c/language/function_declaration>.
    </footnote>. The function prototype parameter names do not have to agree
    with the function definition parameter names<\footnote>
      K&R 2nd Ed., Sec 1.7 \PFunctions\Q.
    </footnote>.

    <label|term garbage_collection><item*|garbage collection>The process of
    freeing memory allocated by a program but which is no longer referenced.
    Usually incurs a significant <hlink|speed
    penalty|https://en.wikipedia.org/wiki/Overhead_(computing)>.
    (<hlink|Wikipedia|https://en.wikipedia.org/wiki/Garbage_collection_(computer_science)>).
    The <name|C> Language does not provide garbage collection by default.

    <item*|<label|term heap>heap>A large pool of memory that can be used
    dynamically \U it is also known as the \Pfree store\Q. This is memory
    that is not automatically managed \U you have to explicitly allocate
    (using functions such as malloc), and deallocate (e.g. free) the memory.
    Failure to free the memory when you are finished with it will result in
    what is known as a memory leak. Is the diametrical opposite of the stack
    (which, by contrast, is limited not by physical memory but by a
    CPU-determined stack size). (See <hlink|craftofcoding.wordpress.com|https://craftofcoding.wordpress.com/2015/12/07/memory-in-c-the-stack-the-heap-and-static/>).

    <item*|<label|term machine-independent>machine-independent>A property of
    code that does not have to be modified in order to run on a different
    hardware architecture. (e.g. \PC is called a portable language because
    [code written in C] will run on any machine which supports C, without
    modifying a single line of code.\Q; <hlink|link|https://www.log2base2.com/C/basic/introduction-to-c-language.html>).

    <item*|<label|term operand>operand>A quantity to which an operator is
    applied. (e.g. in the <name|C> math expression <code*|7 - 4 = 3>,
    <code*|7> is the first operand and <code*|4> is the second operand.)

    <item*|<label|term operator>operator>A special type of function with
    limited numbers of parameters (e.g. 1 to 2) and syntax often requiring a
    set of characters different from those normally use for naming variables
    (e.g. the <code*|+> character in <code*|myVar = 1 + 2>, the <code*|&&> in
    <code*|a && b>, or the <code*|++> in <code*|i++>.).

    <label|term_parameter><item*|parameter>Generally, a variable named in the
    parenthesized list in a <strong|function> definition. Also known as a
    \Pformal argument\Q as opposed to an \Pactual argument\Q (e.g.
    <strong|argument>). See <em|argument>.

    <item*|<label|term stack>stack>A region of memory for global variable
    storage and is permanent for the entire run of the program. Stack size is
    limited not by physical memory availability but by the CPU. Lifetime of
    local variables declared within the stack is enforced by the Last-In,
    First-Out nature of the stack; when a function returns a value, all stack
    memory allocated by declarations within the function is automatically
    freed. (See <hlink|craftofcoding.wordpress.com|https://craftofcoding.wordpress.com/2015/12/07/memory-in-c-the-stack-the-heap-and-static/>)

    <label|term_statement><item*|statement>An <strong|expression> when it is
    followed by a semicolon. May be grouped together using braces (i.e.
    \P<cpp|{ }>\Q) to form a <em|compound statement> or <em|block> which is
    syntactically equivalent to a single <strong|statement>.<\footnote>
      K&R 2nd Ed., Sec. 3.1 \PStatements and Blocks\Q.
    </footnote>

    <item*|<label|term struct>struct>(short:
    <hlink|struct|https://en.wikipedia.org/wiki/struct_(C_programming_language)>)
    a <hlink|composite data type|https://en.wikipedia.org/wiki/Composite_data_type>
    that defines a physically grouped list of variables under one name in a
    block of memory, allowing the different variables to be accessed via a
    single <hlink|pointer|https://en.wikipedia.org/wiki/Pointer_(computer_programming)>
    or by the struct declared name which returns the same address.

    <label|term structure_assignment><item*|structure assignment>The act of
    <hlink|assigning|https://en.wikipedia.org/wiki/Assignment_(computer_science)>
    a <hlink|struct|https://en.wikipedia.org/wiki/struct_(C_programming_language)>
    to another struct. (?) (<hlink|example|https://stackoverflow.com/a/2302359/10850071>)

    <item*|<label|term text_stream>text stream>A sequence of characters
    divided into lines; each line consists of zero or more characters
    followed by a newline character. (See K&R 2nd Ed. Section 1.5 \PCharacter
    Input and Output\Q)

    <label|term_type><item*|type>A way of differentiating data stored for use
    in a program. Some types include <cpp|int> (integers), <cpp|char>
    (single-byte characters), <cpp|short> (a short integer), <cpp|long> (a
    long integer), <cpp|double> (a double-precision floating point number).
  </description-compact>

  \;

  <section|Functions>

  <subsection|Character Input and Output>

  <\description>
    <item*|<cpp|getchar()><label|func getchar>>Read one character at a time
    from the input buffer. Returns as <cpp|int> a <em|character
    constant>.<\footnote>
      <name|ASCII> characters have constants in the range <math|0-127>. UTF-8
      characters return multiple constants (e.g. \<#B0\> returns <cpp|194
      176>). See <hlinkv|https://www.smashingmagazine.com/2012/06/all-about-unicode-utf8-character-sets/>.
      A strategy for reading the multiple bytes of a UTF-8 character is here:
      <hlinkv|https://stackoverflow.com/a/34240796/>.
    </footnote>

    <item*|<cpp|for()>>A generalization of <cpp|while()>. Takes three
    arguments:

    <\enumerate>
      <item>Local statement to run before loop (e.g. to initialize a counting
      variable).

      <item>Local statement that, if evaluated as true, permits running of
      next iteration of loop.

      <item>Local statement to run after loop (e.g. to increment a counting
      variable).
    </enumerate>

    <item*|<cpp|putchar(int arg1)><label|func putchar>>Write one integer
    character (e.g. <cpp|arg1>) at a time.

    <item*|<cpp|printf()>>Used for printing formatted text to console.

    Character codes include:

    <\description-aligned>
      <item*|<cpp|%c>>Used to display a character by its <cpp|int>.

      <item*|<cpp|%d>>Used with <cpp|int> (i.e. 16-bit integers; max value of
      <math|2<rsup|16>=32\<space\>767>).

      <item*|<cpp|%ld>>Used with <cpp|long> (i.e. at least 32-bit
      integers).<\footnote>
        K&R 2nd Ed., Sec. 1.5 \PThe conversion specification <cpp|%ld> tells
        <cpp|printf> that the corresponding argument is a <cpp|long>
        integer.\Q
      </footnote>

      <item*|<cpp|%f>>Used with <cpp|float> and <cpp|double> (double
      precision <cpp|float>).
    </description-aligned>

    For printing <name|UTF-8> strings (which are multibyte in contrast to
    one-byte <name|ASCII> strings), functions from the C standard library
    <cpp|wchar.h> header file<\footnote>
      See <hlinkv|https://en.wikibooks.org/wiki/C_Programming/wchar.h>.
    </footnote> may need to be<\footnote>
      See <hlinkv|https://linuxprograms.wordpress.com/tag/c-utf-8-handling/>.
    </footnote> used<\footnote>
      See <hlinkv|https://stackoverflow.com/questions/56756204/compatibility-of-printf-with-utf-8-encoded-strings>.
    </footnote> (e.g. <cpp|wprintf()> (?)).
  </description>

  \;
</body>

<\initial>
  <\collection>
    <associate|page-medium|papyrus>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-2|<tuple|1.1|1|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-3|<tuple|1.2|4|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|auto-4|<tuple|1.2.1|4|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnote-1.1.1|<tuple|1.1.1|1|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnote-1.1.10|<tuple|1.1.10|3|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnote-1.1.11|<tuple|1.1.11|3|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnote-1.1.2|<tuple|1.1.2|1|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnote-1.1.3|<tuple|1.1.3|2|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnote-1.1.4|<tuple|1.1.4|2|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnote-1.1.5|<tuple|1.1.5|2|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnote-1.1.6|<tuple|1.1.6|2|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnote-1.1.7|<tuple|1.1.7|2|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnote-1.1.8|<tuple|1.1.8|2|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnote-1.1.9|<tuple|1.1.9|3|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnote-1.2.1|<tuple|1.2.1|4|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnote-1.2.2|<tuple|1.2.2|4|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnote-1.2.3|<tuple|1.2.3|4|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnote-1.2.4|<tuple|1.2.4|4|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnote-1.2.5|<tuple|1.2.5|4|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnr-1.1.1|<tuple|1.1.1|1|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnr-1.1.10|<tuple|1.1.10|3|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnr-1.1.11|<tuple|1.1.11|3|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnr-1.1.2|<tuple|1.1.2|1|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnr-1.1.3|<tuple|1.1.3|2|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnr-1.1.4|<tuple|1.1.4|2|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnr-1.1.5|<tuple|1.1.5|2|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnr-1.1.6|<tuple|1.1.6|2|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnr-1.1.7|<tuple|1.1.7|2|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnr-1.1.8|<tuple|1.1.8|2|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnr-1.1.9|<tuple|1.1.9|3|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnr-1.2.1|<tuple|1.2.1|4|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnr-1.2.2|<tuple|1.2.2|4|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnr-1.2.3|<tuple|1.2.3|4|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnr-1.2.4|<tuple|1.2.4|4|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|footnr-1.2.5|<tuple|1.2.5|4|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|func getchar|<tuple|1.2.1|4|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|func putchar|<tuple|3|4|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|ref includehelp-oct-hex|<tuple|1.1.3|2|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|term declaration|<tuple|call|1|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|term definition|<tuple|1.1.1|1|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|term enumeration|<tuple|linter|1|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|term escape_sequence|<tuple|enumeration|1|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|term garbage_collection|<tuple|1.1.10|3|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|term heap|<tuple|garbage collection|3|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|term machine-independent|<tuple|<label|term
    heap>heap|3|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|term operand|<tuple|<label|term
    machine-independent>machine-independent|3|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|term operator|<tuple|<label|term
    operand>operand|3|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|term stack|<tuple|parameter|3|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|term struct|<tuple|1.1.11|3|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|term structure_assignment|<tuple|<label|term
    struct>struct|3|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|term text_stream|<tuple|structure
    assignment|3|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|term_argument|<tuple|1.1|1|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|term_call|<tuple|assignment|1|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|term_expression|<tuple|1.1.4|2|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|term_function|<tuple|1.1.8|3|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|term_function_call|<tuple|function|3|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|term_function_prototype|<tuple|function
    call|3|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|term_linter|<tuple|1.1.2|1|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|term_parameter|<tuple|<label|term
    operator>operator|3|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|term_statement|<tuple|<label|term
    stack>stack|3|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
    <associate|term_type|<tuple|<label|term text_stream>text
    stream|4|../../../../.TeXmacs/texts/scratch/no_name_10.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Definitions>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      1.1<space|2spc>Terminology <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>

      1.2<space|2spc>Functions <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>

      <with|par-left|<quote|1tab>|1.2.1<space|2spc>Character Input and Output
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4>>
    </associate>
  </collection>
</auxiliary>