/* Desc: While Loops
 * Usage: ./test
 * Ref/Attrib: [0] C programming tutorial for beginners https://youtu.be/KJgsSFOSQv0?t=8985
 *             [1] https://youtu.be/ix5jPkxsr7M?t=3072
 *             [2] C: Multiple scanf's, when I enter in a value for one scanf it skips the second scanf https://stackoverflow.com/a/9562355
 */


#include <stdio.h>
#include <stdlib.h>

int main(){

  int index;

  index = 6;
  do {
    printf("index: %d\n", index);
    //index = index + 1;
    index++;
  } while( index <= 5 );
  printf("Done.\n\n");

  index = 5;
  do {
    printf("index: %d\n", index);
    //index = index + 1;
    index++;
  } while( index <= 5 );
  printf("Done.\n\n");
  
  return 0;
};




/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
