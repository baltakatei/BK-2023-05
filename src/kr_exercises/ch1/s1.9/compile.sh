#!/usr/bin/env bash
# Desc: Check sytnax and compiles file using gcc
# Usage: compile.sh mysource.c

yell() { echo "$0: $*" >&2; } # print script path and all args to stderr
die() { yell "$*"; exit 111; } # same as yell() but non-zero exit status
try() { "$@" || die "cannot $*"; } # runs args as command, reports args if command fails
main() {
    # Check arg
    if [[ $# -ne 1 ]]; then die "FATAL:Incorrect argument count:$#"; fi;
    if [[ ! -f $1 ]]; then die "FATAL:Not a file:$1"; else f="$1"; fi;

    # Set var names
    f="$(readlink -f $f)"; fe="${f%.c}";
    yell "DEBUG:f :$f";
    yell "DEBUG:fe:$fe";

    # Check syntax
    if ! try gcc -fsyntax-only "$f"; then die "FATAL:Syntax Error."; fi;

    # Compile
    time { try gcc -o "$fe" "$f"; };

    return 0;
}; # main program

main "$@";

