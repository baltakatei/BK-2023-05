#include <stdio.h>

/* Exercise 1-17. Write a program to print all input lines that are
   longer than 80 characters. */

/* Strat: Read characters until 1) a newline character is encountered,
   2) EOF is encountered, or 3) MAXLINE characters are read from the
   most recent line.

   If a newline character is encountered, check if line's character
   count is greater than 80. If so, print the line.

   If an EOF is encountered, check if the line's character count is
   greater than 80. If so, print the line.

   If the line has more than MAXLINE characters, then print only
   MAXLINE characters.

*/

#define MINLINE 80  /* minimum line length required to be output */
#define MAXLINE 90 /* maximum input line size */

int mygetline(char line[], int maxline); /* 'my-' to avoid name collision */

int main() {
  int len;             /* working line length */
  char line[MAXLINE];  /* working line*/

  /* Read lines and line lengths */
  while ((len = mygetline(line, MAXLINE)) > 0) {

    printf("DEBUG:%d\n",len);
    /* Check if line length > MINLINE */
    if ( len >= MINLINE ) {
      printf("OUTPUT:%s\n", line);
    };
    printf("DEBUG:end loop =============================\n");
  };
  printf("DEBUG:End of main()==========================\n");
  return 0;
};

/* mygetline: read a line into s, \0-term, return length v2 */
int mygetline(char s[], int lim) {
  
  int c, i, k, j;

  /* Read chars into s[] until lim reached */
  for (i=0; i<lim-1 && (c=getchar())!=EOF && c!='\n'; ++i )
    s[i] = c;
  j = i; /* Store char count for counting beyond lim */

  /* \0 terminate array s[] */
  if (c=='\n') {
    s[i] = c;
    ++i;
  };
  s[i] = '\0';
  
  /* Count up remaining chars beyond lim until EOF or '\n' reached */
  if (j>=lim-1) {
    while ( (c=getchar()) != EOF && c != '\n')
      ++j;
  };
  if (c=='\n')
    ++j;

  /* Return char count (including chars beyond array limit)*/
  return j;
};
