#include <stdio.h>

/* Prints a staircase made of characters. */

#define HEIGHT 140

int main() {
  for(int i=1; i<=HEIGHT; i++) {
    for(int j=i; j>0; j--) {
      printf("%d",i % 10);
      if (j==1)
        printf("\n");
    };
  };
  return 0;
};
