#include <stdio.h>

#define MAXLINE 1000

int mygetline(char line[], int maxline); /* 'my-' to avoid name collision */

int main() {
  int len;
  char line[MAXLINE];

  while( (len=mygetline(line, MAXLINE)) > 0 ) {
    line[0]=64; // '@'
    printf("%s\n", line);
  };
  
  return 0;
};

/* mygetline: read a line into s, \0-term, return length v2 */
int mygetline(char s[], int lim) {
  int c, i, k, j;

  /* Read chars into s[] until lim reached */
  for (i=0; i<lim-1 && (c=getchar())!=EOF && c!='\n'; ++i )
    s[i] = c;
  j = i; /* Store char count for counting beyond lim */

  /* \0 terminate array s[] */
  if (c=='\n') {
    s[i] = c;
    ++i;
  };
  s[i] = '\0';
  
  /* Count up remaining chars beyond lim until EOF or '\n' reached */
  if (j>=lim-1) {
    while ( (c=getchar()) != EOF && c != '\n')
      ++j;
  };
  if (c=='\n')
    ++j;

  /* Return char count (including chars beyond array limit)*/
  return j;
};
