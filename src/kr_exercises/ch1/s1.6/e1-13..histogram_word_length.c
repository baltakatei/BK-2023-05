#include <stdio.h>

/* Exercise 1-13. Write a program to print a histogram of the length
   of words in its input. It is easy to draw the histogram with the
   bars horizontal; a vertical orientation is more challenging. */

#define MAX_LEN      20  /* Max acceptable word length */
#define MAX_DISP_LEN 70  /* Max length of horizontal bar */
#define IN            1  /* Inside a word */
#define OUT           0  /* Outside a word */

int main() {
  int n, c, i, j, state, nwchar;
  int wlen[ MAX_LEN + 1 ];

  state = OUT;
  nwchar = n = 0;
  for (int i = 0; i <= MAX_LEN; ++i)
    wlen[i] = 0;

  /* Populate wlen[] */
  while ( (c = getchar()) != EOF ) {
    n++;  /* Track loop number. */
    /* Check if whitespace */
    if ( c == ' ' || c == '\n' || c == '\t' ) {
      /* printf("%d:DEBUG:In whitespace.\n", n); */
      /* I'm in whitespace */
      /* Check if I just departed a word */
      if ( state == IN ) {
        /* Increment appropriate element of wlen[] */
        if ( nwchar <= MAX_LEN )
          ++wlen[nwchar];
        else
          ++wlen[0]; /* Use zero-index for words longer than MAX_LEN */
      };
      /* Remember I'm not in a word */
      state = OUT;
      nwchar = 0;
    } else {
      /* printf("%d:DEBUG:In word.\n", n); */
      /* I'm in a word. */
      /* Check if I just departed whitespace*/
      if ( state == OUT ) {
        /* I just started a word. */
        nwchar = 1;
      } else {
        /* I'm in a word. */
        ++nwchar;        
      }

      /* Remember I'm in a word. */
      state = IN;
    }

    /* End of while loop stats */
    /* printf("%d:DEBUG:nwchar:%d\n", n, nwchar);    */
  }

  
  /* /\* DEBUG:display wlen[] *\/ */
  /* printf("wlen[]:");   */
  /* for (i = 0; i < MAX_LEN; ++i) */
  /*   printf(" %d", wlen[i]); */
  /* printf("\n"); */

  /* int tn = 0; */
  /* ++tn; */
  /* printf("wlen[%d]:%d\n", tn, wlen[tn]); */

  
  /* Display wlen[] contents */
  /* Iterate through elements of wlen[] (skipping zeroth element) */
  for (i = 1; i <= MAX_LEN; ++i) {
    /* Print bar for i'th entry in wlen[] */
    printf("%d: ", i);
    for (j = 0; j < wlen[i]; ++j ) {
      if ( j < MAX_DISP_LEN )
        printf("#");
    }
      
    printf("\n");
  };

  /* Print bar for 0'th entry in wlen[] (words exceeding MAX_LEN) */
  printf("%d+: ", MAX_LEN + 1 );
  for (j = 0; j < wlen[0]; ++j )
    if ( j < MAX_DISP_LEN )
      printf("#");
  printf("\n");
  
  return 0;
};

/* Author: Steven Baltakatei Sandoval
   License: GPLv3+ */
