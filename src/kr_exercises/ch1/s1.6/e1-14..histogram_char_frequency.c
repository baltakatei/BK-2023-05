#include <stdio.h>

/* Exercise 1-14. Write a program to print a histogram of the
   frequencies of different characters in its input. */

#define INT_CHAR_MIN   32
#define INT_CHAR_MAX  126
#define INT_CHAR_LEN  INT_CHAR_MAX - INT_CHAR_MIN + 1
#define INT_NEWLINE    10


int main() {
  int c, counts_idx, flg_intchar_err;
  int counts[INT_CHAR_LEN];
  for(int i = 0; i < INT_CHAR_LEN; ++i)
    counts[i] = 0;

  
  /* Read char from input; Ignore newlines. */
  flg_intchar_err = 0;
  while( (c = getchar()) != EOF ) {
    if( c >= INT_CHAR_MIN && c <= INT_CHAR_MAX ) {
      /* Increment corresponding element in counts[] array */
      counts_idx = c - INT_CHAR_MIN;
      ++counts[counts_idx];
    } else if ( c != INT_NEWLINE )
      flg_intchar_err = 1;
  };

  /* Print contents of counts[i] */
  printf("counts[i]:(ASCII code):( symbol ): count : histogram bar\n");
  for(int i = 0; i < INT_CHAR_LEN; ++i) {
    printf("counts[%03d]:(%03d):( %c ):%03d:",
           i, i + INT_CHAR_MIN, i + INT_CHAR_MIN, counts[i]);
    for(int j = 0; j < counts[i]; ++j)
      printf("#");
    printf("\n");
  };
    

  /* Report input errors */
  if( flg_intchar_err == 1 )
    printf("ERROR:Unrecognized char int provided.");
  return 0;
};

/* Author: Steven Baltakatei Sandoval
   License: GPLv3+ */
