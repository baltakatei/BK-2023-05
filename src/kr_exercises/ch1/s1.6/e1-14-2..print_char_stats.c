#include <stdio.h>

/* Prints stats of chars provided. */

int main() {
  int c, n, cmin, cmax;

  n = 0;
  cmin = cmax = -1;
  while( (c = getchar()) != EOF) {
    if( n == 0 )
      cmin = cmax = c;

    if( c != 10 ) {
      if( c < cmin )
        cmin = c;
      else if (c > cmax)
        cmax = c;
    };

    n++;
  };

  printf("char count  :%d\n", n   );
  printf("char int min:%d\n", cmin);
  printf("char int max:%d\n", cmax);
};
