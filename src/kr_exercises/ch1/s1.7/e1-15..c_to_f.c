#include <stdio.h>

int f_to_c(int degf);

/* print Fahrenheit-Celsius table for fahr = 0, 20, ..., 300 */

int main() {
  int fahr, celsius;
  int lower, upper, step;

  lower = 0;   /* lower limit of temperature table */
  upper = 300; /* upper limit */
  step = 20;   /* step size */

  fahr = lower;
  while (fahr <= upper) {
    celsius = f_to_c(fahr);
    /* celsius = 5 * (fahr-32) / 9; */
    printf("%d\t%d\n", fahr, celsius);
    fahr = fahr + step;
  };
};

/* f_to_c: calculate celsius from fahrenheit */
int f_to_c(int degf) {
  int degc;
  degc = 5 * (degf - 32) / 9;
  return degc;
};

/* Author: Steven Baltakatei Sandoval
   License: GPLv3+
*/
