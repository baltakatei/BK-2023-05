/* Desc: simple temperature converter
 * Usage: ./s1..c_to_f_float.c
 * Ref/Attrib: The C Programming Language, 2nd Edition, Section 1.2
 */

#include <stdio.h>
//#include <stdlib.h>

/* print Celsius-Fahrenheit table for celsius = -50, -40, ..., 100 */

int main(){
  float fahr, celsius;
  int lower, upper, step;

  lower = -50;   /* lower limit of temperature table */
  upper = 100;   /* upper limit */
  step  =   5;   /* step size */

  celsius = lower;
  printf(" °C,    °F\n");
  while (celsius <= upper){
    fahr = (celsius * (9.0 / 5.0))  + 32.0;
    printf("%3.0f,%7.2f\n", celsius, fahr);
    celsius = celsius + step;
  };

  
  printf("\n\nDone\n\n");
  return 0;
}




/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
