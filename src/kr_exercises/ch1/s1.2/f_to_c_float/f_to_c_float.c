/* Desc: simple temperature converter
 * Usage: ./s1..f_to_c_float.c
 * Ref/Attrib: The C Programming Language, 2nd Edition, Section 1.2
 */

#include <stdio.h>
//#include <stdlib.h>

/* print Fahrenheit-Celsius table for fahr = 0, 20, ..., 300 */

int main(){
  float fahr, celsius;
  int lower, upper, step;

  lower =   0;   /* lower limit of temperature table */
  upper = 300;   /* upper limit */
  step  =  20;   /* step size */

  fahr = lower;
  printf(" °F,    °C\n");
  while (fahr <= upper){
    celsius = (5.0/9.0) * (fahr-32.0);
    printf("%3.0f,%6.1f\n", fahr, celsius);
    fahr = fahr + step;
  };

  
  printf("\n\nDone\n\n");
  return 0;
}

/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
