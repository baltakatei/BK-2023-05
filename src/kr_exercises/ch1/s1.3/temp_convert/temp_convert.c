#include <stdio.h>
/* Desc: Prints Fahrenheit-Celsius table
 * Usage: name=temp_convert; gcc -o "$name" "$name".c; ./"$name";
 * Ref/Attrib: [0]: The C Programming Language, 2nd Edition, Section 1.2
 */

int main(){
  int fahr;
  for (fahr = 0; fahr <= 300; fahr = fahr + 20)
    printf("%3d %6.1f\n", fahr, (5.0/9.0)*(fahr - 32) );
  return 0;
}

/* Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
