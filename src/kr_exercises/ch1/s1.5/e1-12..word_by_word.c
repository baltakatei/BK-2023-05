#include <stdio.h>

#define IN  1
#define OUT 0

/* Prints input one word per line. */

int main() {
  int c, state;

  while( (c = getchar()) != EOF ) {
    if ( c == ' ' || c == '\n' || c == '\t' ) {
      if ( state == IN )
        printf("\n");
      state = OUT;
    } else {
      putchar(c);
      state = IN;
    };
  };

  printf("\n");
  return 0;
};
