/* Desc: Counts blanks, tabs, and newlines */
/* Ref/Attrib: K&R 2nd Ed. Exercise 1-8 */

#include <stdio.h>
int main() {
  int c, cb, ct, cnl;

  /* Initialize counters */
  cb = 0;
  ct = 0;
  cnl = 0;

  /* User prompt loop. */
  while((c = getchar()) != EOF) {
    /* Detect blanks character */
    if(c == ' ')
      ++cb;
    
    /* Detect tab character */
    if(c == '\t')
      ++ct;

    /* Detect newline character */
    if(c == '\n')
      ++cnl;
  };

  /* Print results. */
  printf("Blnk:\t%d\n", cb);
  printf("Tabs:\t%d\n", ct);
  printf("Nwln:\t%d\n", cnl);
  printf("\nDone.\n");
};

/* Author: Steven Baltakatei Sandoval
   License: GPLv3+ */
