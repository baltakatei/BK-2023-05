/* Desc: Print value of EOF
 * Usage: name=print_eof; gcc -o "$name" "$name".c; ./"$name";
 * Ref/Attrib: [0]: The C Programming Language, 2nd Edition, Section 1.5.1 Exercise 1.7

 * Note: This program should print the integer value encoding EOF as
 * specified in `<stdio.h>`.
 */
#include <stdio.h>


int main(){
  
  int c;
  c = EOF;
  printf("%d\n", c);
  
  return 0;
}

/* Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
