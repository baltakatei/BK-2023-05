/* Desc: a program that copies its input to output
 * Usage: name=read_and_output; gcc -o "$name" "$name".c; ./"$name";
 * Ref/Attrib: [0]: The C Programming Language, 2nd Edition, Section 1.5.1
 */
#include <stdio.h>

/* copy input to output; 1st version */
int main(){
  int c;

  c = getchar();    // read a character
  while(c != EOF) {
    putchar(c);     // output the character just read
    c = getchar();  // read a character
  }
  return 0;
}

/* Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
