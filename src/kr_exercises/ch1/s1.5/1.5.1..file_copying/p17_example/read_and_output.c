/* Desc: a program that copies its input to output
 * Usage: name=read_and_output; gcc -o "$name" "$name".c; ./"$name";
 * Ref/Attrib: [0]: The C Programming Language, 2nd Edition, Section 1.5.1 p17
 */
#include <stdio.h>

/* copy input to output; 2nd version */
int main(){
  int c;

  while( ( c = getchar() ) != EOF )  // read characters until EOF detected
    putchar(c);                      // output the character just read

  return 0;
}

/* Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
