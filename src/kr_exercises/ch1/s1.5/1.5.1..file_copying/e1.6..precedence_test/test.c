/* Desc: Tests the precedence of = and != 
 * Usage: name=read_and_output; gcc -o "$name" "$name".c; ./"$name";
 * Ref/Attrib: [0]: The C Programming Language, 2nd Edition, Section 1.5.1 Exercise 1.6

 * Note: After starting the program, type several characters and then
 * press Enter. The integer `1` should be printed, indicating the `!=`
 * expression resulted in a “true” result, encoded as a boolean flag
 * of the integer `1` or `0`. Restart the program. Instead of typing
 * any character, type Ctrl-d, which should be the EOF character for
 * most terminals. The integer `0` should be printed, indicating that
 * the int returned by the expression `getchar() != EOF` was `0`,
 * indicating the != operation resulted in a “false” result.
 */
#include <stdio.h>


int main(){
  int c;
  
  c = getchar() != EOF;
  printf("%d\n", c);
  
  return 0;
}

/* Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
