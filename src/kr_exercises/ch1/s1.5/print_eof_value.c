/* Desc: Prints value of EOF
 * Usage: name=print_eof_value; gcc -o "$name" "$name".c; ./"$name";
 * Ref/Attrib: [0]: The C Programming Language, 2nd Edition, Section 1.5.1
 */

/* Exercise 1-7 Prints the value of EOF specified by stdio.h
 */
#include <stdio.h>

int main(){
  int output;
  output = EOF;
  printf("EOF value:%d", output);
  return 0;
};

/* Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
