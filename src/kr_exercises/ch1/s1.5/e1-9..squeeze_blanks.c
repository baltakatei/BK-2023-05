/* Desc: Squeezes consecutive blanks */
/* Ref/Attrib: K&R 2nd Ed. Exercise 1-9 */

#include <stdio.h>
int main() {  
  int c, cb;

  /* Initialize counters */
  cb = 0; // count of blanks

  /* User prompt loop. */
  while((c = getchar()) != EOF) {

    if(c == ' ') {
      /* If c is blank */
      ++cb;
    } else if (cb <= 0) {
      /* If last round was not a blank AND c is not a blank */
      putchar(c);
    } else {
      /* Last round was a blank AND c is not a blank */ 
      cb = 0;
      printf(" ");
      putchar(c);
    };
  };
  
  if(cb > 0) {
    /* If EOF received after blank(s) */
    printf(" ");
  };

  printf("\nDone.\n");
};

/* Author: Steven Baltakatei Sandoval
   License: GPLv3+ */
