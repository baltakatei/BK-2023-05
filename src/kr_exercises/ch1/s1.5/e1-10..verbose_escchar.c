/* Desc: Print tabs, backspaces, and backslashes with escape backslashes */
/* Ref/Attrib: K&R 2nd Ed. Exercise 1-10 */

#include <stdio.h>
int main() {
  int c;

  /* User prompt loop. */
  while((c = getchar()) != EOF) {

    if(c == '\t') {
      /* If c is tab */
      printf("\\t");
    } else if(c == '\b') {
      /* If c is backspace */
      printf("\\b");
    } else if(c == '\\') {
      /* If c is backslash */
      printf("\\\\");
    } else
      putchar(c);
    
  };

  printf("\nDone.\n");
};

/* Author: Steven Baltakatei Sandoval
   License: GPLv3+ */
