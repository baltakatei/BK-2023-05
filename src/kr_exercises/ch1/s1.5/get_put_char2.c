/* Desc: Reads and prints inputted char
 * Usage: name=get_put_char2; gcc -o "$name" "$name".c; ./"$name";
 * Ref/Attrib: [0]: The C Programming Language, 2nd Edition, Section 1.5.1
 */
#include <stdio.h>

/* copy input to output; 2nd version */

int main(){
  int c;

  while((c = getchar()) != EOF)
    putchar(c);
  return 0;
};

/* Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
