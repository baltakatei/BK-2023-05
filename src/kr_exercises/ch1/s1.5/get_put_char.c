/* Desc: Reads and prints inputted char
 * Usage: name=get_put_char; gcc -o "$name" "$name".c; ./"$name";
 * Ref/Attrib: [0]: The C Programming Language, 2nd Edition, Section 1.5.1
 */
#include <stdio.h>

/* copy input to output; 1st version */

int main(){
  int c;

  c = getchar();
  while(c != EOF) {
    putchar(c);
    c = getchar();
  };
  return 0;
};

/* Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
