#include <stdio.h>

/* Returns character constant of first provided char.*/
int main() {
  int c;

  while ((c = getchar()) != EOF) {
    if (c == '\n') {
      printf("\n");
    } else {
      printf("%d\n", c);
    };

  };

};
