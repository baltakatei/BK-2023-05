/* Desc: Test behavior of "getchar() not equal to EOF"
 * Usage: name=getchar_eof_test; gcc -o "$name" "$name".c; ./"$name";
 * Ref/Attrib: [0]: The C Programming Language, 2nd Edition, Section 1.5.1
 */
#include <stdio.h>

/* Exercise 1-6 solution: Reads a single character. Will display
 *  "output:0" if first character provided is EOF (e.g. "alt+D").
 *  Otherwise, will display "output:1".
 */

int main(){
  int output;
  output = (getchar() != EOF);
  printf("output:%d", output);
  return 0;
};

/* Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
