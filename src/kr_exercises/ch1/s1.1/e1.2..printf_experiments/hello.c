#include <stdio.h>
int main()
{
  printf("hello\n");  
  printf("\v");        // Vertical tab
  printf("world\?\n"); // Question mark
  printf("foo\x0A");   // Newline (hexadecimal)
  printf("ba\br\n");   // Backspace
  printf("baz\b\n");   // Backspace moves cursor back but doesn't overwrite z
  printf("baz\b \n");  // Backspace overwrites z with space
}
