/* Desc: Prints Fahrenheit-Celsius table; uses symbolic constants
 * Usage: name=temp_convert; gcc -o "$name" "$name".c; ./"$name";
 * Ref/Attrib: [0]: The C Programming Language, 2nd Edition, Section 1.4
 */
#include <stdio.h>

# define LOWER   0  /* Lower limit of table. */
# define UPPER 300  /* Upper limit of table. */
# define STEP   20  /* Step size. */

/* Print Fahrenheit–Celsius table */

int main(){
  int fahr;
  for (fahr = LOWER; fahr <= UPPER; fahr = fahr + STEP)
    printf("%3d %6.1f\n", fahr, (5.0/9.0)*(fahr - 32) );
  return 0;
}

/* Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
