/* Desc: Prints hello world.
 */

#include <stdio.h>
int main() {
  int a = 10;
  printf("Hello world! \n");
  printf("The value of a : %d", a);
  getchar(); // pause until user hits 'enter'
  return 0;
};

/* Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
