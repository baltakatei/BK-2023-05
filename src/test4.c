/* Desc: Test for loop.
 * Usage: test4
 * Ref/Attrib: [1] https://youtu.be/ix5jPkxsr7M?t=3072
 */

#include <stdio.h>

int main(void)
{
  char my_string[1] = "abcd";  
  //  string name = get_string("What is your name?\n:");
  //  printf("Hello, name\n");
  printf("my_string:%s\n", my_string);
}

/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
