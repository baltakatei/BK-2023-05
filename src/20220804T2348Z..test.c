/* Desc:
 * Usage: ./test
 * Ref/Attrib: [1] https://youtu.be/ix5jPkxsr7M?t=3072
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{

  int num = 6;
  printf("%f\n", 3.141592); // 'f' for floating-point number
  printf("%f\n", 5.0 * 9.5); // 'f' for floating-point number
  printf("%f\n", 5 * 9.5); // int + float results in float
  printf("%d\n", 5 / 4); // doesn't show decimals
  printf("%f\n", 5 / 4.0);
  printf("%d\n", num); // print an int variable
  printf("%f\n", pow(4, 3)); // exponentiation; requires <math.h> to be included
  printf("%f\n", sqrt(36)); // square root
  printf("%f\n", ceil(36.356)); // ceiilng; rounds up
  printf("%f\n", floor(36.356)); // ceiilng; rounds down
  
  printf("\n");
  return 0;
};

/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
