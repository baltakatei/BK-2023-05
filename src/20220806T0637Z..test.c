/* Desc: If Statements
 * Usage: ./test
 * Ref/Attrib: [0] C programming tutorial for beginners https://youtu.be/KJgsSFOSQv0?t=6802
 *             [1] https://youtu.be/ix5jPkxsr7M?t=3072
 *             [2] C: Multiple scanf's, when I enter in a value for one scanf it skips the second scanf https://stackoverflow.com/a/9562355
 */


#include <stdio.h>
#include <stdlib.h>

int max(int num1, int num2, int num3){
  // Desc: Return the bigger of the three input integers
  // Input: int num1
  //        int num2
  //        int num3
  // Output: int result
  
  int result;
  if(num1 >= num2 && num1 >= num3){
    result = num1;
  } else if(num2 >= num1 && num2 >= num3) {
    result = num2;
  } else {
    result = num3;
  };
  return result;  
};

int main(){

  if(3 > 2 || 2 > 5){
    printf("True\n");
  } else {
    printf("False\n");
  };

  if(3 < 2 || 2 > 5){
    printf("True\n");
  } else {
    printf("False\n");
  };

  if(3 == 2){
    printf("Three equals two.\n");
  };

  if(3 != 2){
    printf("Three is not equal to two.\n");
  };

  // negation operator ("!")exaple
  if(!(3 < 2)){
    printf("True");
  }
  
  printf("%d", max(700, 13, 17));
  
  return 0;
};




/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
