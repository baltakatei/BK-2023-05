/* Desc: Hello world.
 * Usage: test3
 * Ref/Attrib: [1] https://youtu.be/ix5jPkxsr7M?t=3072
 */

#include <stdio.h>
int main(void)
{
  printf("Hello, world.\n");
}

/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
