/* Desc: Reading Files
 * Usage: ./test
 * Ref/Attrib: [0] C programming tutorial for beginners https://youtu.be/KJgsSFOSQv0?t=3h41m54s
 *             [1] https://youtu.be/ix5jPkxsr7M?t=3072
 *             [2] C: Multiple scanf's, when I enter in a value for one scanf it skips the second scanf https://stackoverflow.com/a/9562355
 *             [3] File Positioning https://www.gnu.org/software/libc/manual/html_node/File-Positioning.html
 */

#include <stdio.h>
//#include <stdlib.h>

int main(){

  int fposition;
  char line[255];
  FILE *fpointer = fopen("employees.txt", "r");

  printf("%p : fpointer address\n", fpointer);
  fgets(line, 255, fpointer);
  fposition = ftell(fpointer); // See [3]
  printf("%d : fposition\n", fposition);

  printf("%p : fpointer address\n", fpointer);  
  printf("%s", line);
  
  printf("%p : fpointer address\n", fpointer);  
  fgets(line, 255, fpointer);
  fposition = ftell(fpointer);
  printf("%d : fposition\n", fposition);
  
  printf("%p : fpointer address\n", fpointer);
  printf("%s", line);

  printf("%p\n : fpointer address\n", fpointer);
  fclose(fpointer);
  printf("\n\nDone.\n\n");
  return 0;
};




/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
