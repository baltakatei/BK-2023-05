/* Desc: Enumeration teste
 * Usage: ./test
 * Ref/Attrib: [5] https://www.geeksforgeeks.org/enumeration-enum-c/
 */

#include <stdio.h>
//#include <stdlib.h>

enum year{Jan, Feb, Mar, Apr, May, Jun, Jul,
    Aug, Sep, Oct, Nov, Dec};

int main(){
  int i;
  for (i=Jan; i<=Dec; i++)
    printf("%d ", i);

  printf("\n\nDone\n\n");
  return 0;
};




/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
