/* Desc: Structs
 * Usage: ./test
 * Ref/Attrib: [0] C programming tutorial for beginners https://youtu.be/KJgsSFOSQv0?t=8487
 *             [1] https://youtu.be/ix5jPkxsr7M?t=3072
 *             [2] C: Multiple scanf's, when I enter in a value for one scanf it skips the second scanf https://stackoverflow.com/a/9562355
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Student{
  char name[50];
  char major[50];
  int age;
  double gpa;
};

int main(){
  struct Student student1; // creates container to store name, major, age, and gpa
  student1.age = 22;
  student1.gpa = 3.2;
  //student1.name = "Jim";  // wont' work
  strcpy( student1.name, "Jim" );
  strcpy( student1.major, "Business" );

  struct Student student2; // creates container to store name, major, age, and gpa
  student2.age = 20;
  student2.gpa = 4.0;
  //student1.name = "Jim";  // wont' work
  strcpy( student2.name, "Pam" );
  strcpy( student2.major, "Chemical Engineering" );
  

  printf("%s\n", student1.name);
  printf("%s\n", student1.major);
  printf("%f\n", student1.gpa);
  printf("%d\n", student1.age);
  printf("\n");

  printf("%s\n", student2.name);
  printf("%s\n", student2.major);
  printf("%f\n", student2.gpa);
  printf("%d\n", student2.age);
  printf("\n");
  
  return 0;
};




/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
