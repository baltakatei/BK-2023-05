/* Desc: calculator to add two integers
 * Usage: ./test
 * Ref/Attrib: [1] https://youtu.be/ix5jPkxsr7M?t=3072
 *             [2] C: Multiple scanf's, when I enter in a value for one scanf it skips the second scanf https://stackoverflow.com/a/9562355
 */


#include <stdio.h>
#include <stdlib.h>


int main()
{
  int num1;
  int num2;
  printf("Enter first number: ");
  scanf("%d", &num1);
  getchar();
  printf("Enter second number: ");
  scanf("%d", &num2);

  printf("Answer: %d", num1 + num2);
  return 0;
};

/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
