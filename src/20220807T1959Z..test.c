/* Desc: 2D Arrays & Nested Loops
 * Usage: ./test
 * Ref/Attrib: [0] C programming tutorial for beginners https://youtu.be/KJgsSFOSQv0?t=2h59m5s
 *             [1] https://youtu.be/ix5jPkxsr7M?t=3072
 *             [2] C: Multiple scanf's, when I enter in a value for one scanf it skips the second scanf https://stackoverflow.com/a/9562355
 */


#include <stdio.h>
#include <stdlib.h>

int main(){

  int nums[3][2] = {
    {1, 2},
    {3, 4},
    {5, 6}
  };

  for(int i = 0; i < 3; i++){
    for(int j = 0; j < 2; j++){
      printf("%d,", nums[i][j]);
    };
    printf("\n");
  };
  
  return 0;
};




/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
