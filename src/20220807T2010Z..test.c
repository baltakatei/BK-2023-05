/* Desc: Memory Addresses
 * Usage: ./test
 * Ref/Attrib: [0] C programming tutorial for beginners https://youtu.be/KJgsSFOSQv0?t=3h9m11s
 *             [1] https://youtu.be/ix5jPkxsr7M?t=3072
 *             [2] C: Multiple scanf's, when I enter in a value for one scanf it skips the second scanf https://stackoverflow.com/a/9562355
 */


#include <stdio.h>
#include <stdlib.h>

int main(){

  int age = 30;
  double gpa = 3.4;
  char grade = 'A';

  printf("age   : %d\n", age);
  printf("gpa   : %f\n", gpa);
  printf("grade : %c\n", grade);
  printf("%p : age\n", &age);
  printf("%p : gpa\n", &gpa);
  printf("%p : grade\n", &grade);
  age = 31;
  gpa = 3.5;
  grade = 'B';
  printf("Done.\n\n");
    
  printf("age   : %d\n", age);
  printf("gpa   : %f\n", gpa);
  printf("grade : %c\n", grade);
  printf("%p : age\n", &age);
  printf("%p : gpa\n", &gpa);
  printf("%p : grade\n", &grade);
  printf("Done.\n\n");
  
  return 0;
};




/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
