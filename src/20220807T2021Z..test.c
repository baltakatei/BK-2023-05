/* Desc: Pointers
 * Usage: ./test
 * Ref/Attrib: [0] C programming tutorial for beginners https://youtu.be/KJgsSFOSQv0?t=3h17m22s
 *             [1] https://youtu.be/ix5jPkxsr7M?t=3072
 *             [2] C: Multiple scanf's, when I enter in a value for one scanf it skips the second scanf https://stackoverflow.com/a/9562355
 */

#include <stdio.h>
//#include <stdlib.h>

int main(){

  int age = 30;
  int * pAge = &age;
  double gpa = 3.4;
  double * pGpa = &gpa;
  char grade = 'A';
  char * pGrade = &grade;
  
  printf("age's memory address: %p\n", &age);
  printf("age's memory address: %p\n", pAge);
  printf("gpa's memory address: %p\n", &gpa);
  printf("gpa's memory address: %p\n", pGpa);
  printf("grade's memory address: %p\n", &grade);
  printf("grade's memory address: %p\n", pGrade);
  
  printf("\n\nDone.\n\n");
  return 0;
};




/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
