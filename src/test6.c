/* Desc:
 * Usage: ./test5
 * Ref/Attrib: [1] https://youtu.be/ix5jPkxsr7M?t=3072
 */

#include <stdio.h>
#include <stdlib.h>

int main()
{
  char characterName[] = "Thomas";
  int characterAge = 67;
  printf("\n");
  printf("There was once a man named %s\n", characterName);
  printf("He was %i years old.\n", characterAge);
  characterAge = 1;
  printf("He really liked the name %s\n", characterName);
  printf("but he did not like being %i.\n", characterAge);
  return 0;
};

/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
