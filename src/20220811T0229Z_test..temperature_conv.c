/* Desc: simple temperature converter
 * Usage: ./test
 * Ref/Attrib: The C Programming Language, 2nd Edition, Section 1.2
 */

#include <stdio.h>
//#include <stdlib.h>

/* print Fahrenheit-Celsius table for fahr = 0, 20, ..., 300 */

int main(){
  int fahr, celsius;
  int lower, upper, step;

  lower = 0; /* lower limit of temperature table */
  upper = 300; /* upper limit */
  step = 20; /* step size */

  fahr = lower;
  while (fahr <= upper){
    celsius = 5 * (fahr-32) / 9;
    printf("%3d %6d\n", fahr, celsius);
    fahr = fahr + step;
  };

  
  printf("\n\nDone\n\n");
  return 0;
};




/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
