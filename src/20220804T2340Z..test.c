/* Desc:
 * Usage: ./test
 * Ref/Attrib: [1] https://youtu.be/ix5jPkxsr7M?t=3072
 */

#include <stdio.h>
#include <stdlib.h>

int main()
{
  int favNum = 90;
  char myChar = 'i';
  printf("\n");
  printf("Hello \"World\".\n");
  printf("%d\n", 500); // 'd' for digit?
  printf("My favorite number is %d\n", 500);
  printf("My favorite %s is %d\n", "number", 500);
  printf("My favorite %s is %f\n", "number", 500.98764); // 'f' for float?
  printf("My favorite %s is %d\n", "number", favNum);
  printf("My favorite character is %c\n", myChar);
  
  printf("\n");
  return 0;
};

/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
