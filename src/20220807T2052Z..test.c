/* Desc: Writing Files
 * Usage: ./test
 * Ref/Attrib: [0] C programming tutorial for beginners https://youtu.be/KJgsSFOSQv0?t=3h32m38s
 *             [1] https://youtu.be/ix5jPkxsr7M?t=3072
 *             [2] C: Multiple scanf's, when I enter in a value for one scanf it skips the second scanf https://stackoverflow.com/a/9562355
 */

#include <stdio.h>
//#include <stdlib.h>

int main(){

  // File modes
  // r : read
  // w : write
  // a : append
  
  FILE * fpointer = fopen( "employees.txt", "w" );
  printf("%p : fpointer address - file opened\n", fpointer);
  fprintf(fpointer, "Jim, Salesman\nPam, Receptionist\nOscar, Accounting\n");
  printf("%p : fpointer address - file written\n", fpointer);
  fclose(fpointer);
  printf("%p : fpointer address - file closed\n", fpointer);

  fpointer = fopen( "employees.txt", "a" );
  printf("%p : fpointer address - file opened\n", fpointer);
  fprintf(fpointer, "Kelly, Customer Service\n");
  printf("%p : fpointer address - file appended\n", fpointer);
  
  


  fclose(fpointer);
  printf("%p : fpointer address - file closed\n", fpointer);
  printf("\n\nDone.\n\n");
  return 0;
};




/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
