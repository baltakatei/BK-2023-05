/* Desc: Building a Guessing Game
 * Usage: ./test
 * Ref/Attrib: [0] C programming tutorial for beginners https://youtu.be/KJgsSFOSQv0?t=9469
 *             [1] https://youtu.be/ix5jPkxsr7M?t=3072
 *             [2] C: Multiple scanf's, when I enter in a value for one scanf it skips the second scanf https://stackoverflow.com/a/9562355
 */


#include <stdio.h>
#include <stdlib.h>

int main(){

  int secretNumber = 5;
  int guess;
  int guessCount = 0;
  int guessLimit = 3;
  int outOfGuesses = 0;

  while( guess != secretNumber && outOfGuesses == 0){
    if( guessCount < guessLimit ){
      printf("Enter a number: ");
      scanf("%d", &guess);
      guessCount++;
    } else {
      outOfGuesses = 1;
    };
  };
  
  if(outOfGuesses == 1){
    printf("Out of guesses");
  } else {
    printf("Hey, you win!\n");
  };
  
  return 0;
};




/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
