/* Desc: Dereferencing Pointers
 * Usage: ./test
 * Ref/Attrib: [0] C programming tutorial for beginners https://youtu.be/KJgsSFOSQv0?t=3h27m43s
 *             [1] https://youtu.be/ix5jPkxsr7M?t=3072
 *             [2] C: Multiple scanf's, when I enter in a value for one scanf it skips the second scanf https://stackoverflow.com/a/9562355
 */

#include <stdio.h>
//#include <stdlib.h>

int main(){

  int age = 30;
  int *pAge = &age;

  printf("%p\n", pAge);  // memory address of age
  printf("%d\n", *pAge); // dereferenced value of memory address of age interpreted as integer
  printf("%d\n", *&age); // dereferenced value of memory address of age interpreted as integer
  printf("%p\n", &*&age); // memory address of dereferenced value of memory address of age interpreted as integer
  printf("%d\n", *&*&age); // dereferenced value of memory address of dereferenced value of memory address of age interpreted as integer
  
  printf("\n\nDone.\n\n");
  return 0;
};




/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
