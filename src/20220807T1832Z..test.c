/* Desc: Building a Better Calculator
 * Usage: ./test
 * Ref/Attrib: [0] C programming tutorial for beginners https://youtu.be/KJgsSFOSQv0?t=7633
 *             [1] https://youtu.be/ix5jPkxsr7M?t=3072
 *             [2] C: Multiple scanf's, when I enter in a value for one scanf it skips the second scanf https://stackoverflow.com/a/9562355
 */


#include <stdio.h>
#include <stdlib.h>

int main(){
  double num1;
  double num2;
  char op; // operator
  printf("Enter a number: ");
  scanf("%lf", &num1);
  printf("Enter operator: ");
  scanf(" %c", &op); // space is important; whitespace to clear input buffer?
  printf("Enter a number: ");
  scanf("%lf", &num2);

  if       ( op == '+' ){
    printf("%f", num1 + num2);
  } else if( op == '-' ){
    printf("%f", num1 - num2);
  } else if( op == '/' ){
    printf("%f", num1 / num2);
  } else if( op == '*' ){
    printf("%f", num1 * num2);
  } else {
    printf("Invalid Operator.\n");
  };
  return 0;
};




/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
