/* Desc: Switch statements
 * Usage: ./test
 * Ref/Attrib: [0] C programming tutorial for beginners https://youtu.be/KJgsSFOSQv0?t=8092
 *             [1] https://youtu.be/ix5jPkxsr7M?t=3072
 *             [2] C: Multiple scanf's, when I enter in a value for one scanf it skips the second scanf https://stackoverflow.com/a/9562355
 */


#include <stdio.h>
#include <stdlib.h>

int main(){
  
  char grade = 'A';
  
  switch(grade){
  case 'A' :
    printf("You did great!\n");
    break;
  case 'B' :
    printf("You did alright!\n");
    break;
  case 'C' :
    printf("You did poorly!\n");
    break;
  case 'D' :
    printf("You did very badly!\n");
    break;
  case 'F' :
    printf("You failed!\n");
    break;
  default :
    printf("Invalid grade.\n");
    return 1;
  };
  
  return 0;
};




/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
