/* Desc: For Loops
 * Usage: ./test
 * Ref/Attrib: [0] C programming tutorial for beginners https://youtu.be/KJgsSFOSQv0?t=10212
 *             [1] https://youtu.be/ix5jPkxsr7M?t=3072
 *             [2] C: Multiple scanf's, when I enter in a value for one scanf it skips the second scanf https://stackoverflow.com/a/9562355
 */


#include <stdio.h>
#include <stdlib.h>

int main(){

  for( int i = 1; i <= 5; i++ ){
    printf("%d\n", i);
  };
  printf("Done.\n\n");

  int luckyNumbers[] = {4, 8, 15, 16, 23, 42};
  for( int i = 0; i < 6; i++ ){
    printf("%d\n", luckyNumbers[i]);
  };
  printf("Done.\n\n");
  
  return 0;
};




/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
