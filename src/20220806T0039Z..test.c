/* Desc: 
 * Usage: ./test
 * Ref/Attrib: [0] C programming tutorial for beginners https://youtu.be/KJgsSFOSQv0?t=5512
 *             [1] https://youtu.be/ix5jPkxsr7M?t=3072
 *             [2] C: Multiple scanf's, when I enter in a value for one scanf it skips the second scanf https://stackoverflow.com/a/9562355
 */


#include <stdio.h>
#include <stdlib.h>


int main()
{
  int luckyNumbers[] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29};
  luckyNumbers[3] = 200;
  printf("%d", luckyNumbers[3]);
  
  return 0;
};

/*
 * Author: Steven Baltakatei Sandoval
 * License: GPLv3+
 */
